/*************************************************************
 * File: Lander.h
 * Author: Br. Burton
 *
 * Description: Contains the definition of the Lander class.
 *
 * Please DO NOT share this code with other students from
 *  other sections or other semesters. They may not receive
 *  the same code that you are receiving.
 *************************************************************/

#ifndef LANDER_H
#define LANDER_H

#include "velocity.h"
#include "point.h"

/*****************************************
 * Lander
 * The main Lander class containing all the state
 *****************************************/
class Lander
{
    private:
        Velocity velocity;
        bool alive;
        bool landed;
        float fuel;
        int inertia;
        Point point;
        float gravity;
    public:
        // Constructors
        Lander();
        // Getters
        Velocity getVelocity() const;
        bool getAlive() const;
        bool getLanded() const;
        float getFuel() const;
        int getInertia() const;
        Point getPoint() const;
        float getGravity() const;
        // Setters
        void setVelocity(float _dx, float _dy);
        void setVelocityX(float _dx);
        void setVelocityY(float _dy);
        void setAlive(bool _alive);
        void setLanded(bool _landed);
        void setFuel(float _fuel);
        void setInertia(int _inertia);
        void setPoint(Point _point);
        void setGravity(float _gravity);
        // Triggers
        void applyGravity(float);
        void applyThrustLeft();
        void applyThrustRight();
        void applyThrustBottom();
        void applyInertia(int _inertia);
        // Control
        bool isAlive();
        bool isLanded();
        bool canThrust();
        void advance();
        void draw();
};

#endif /* LANDER_H */

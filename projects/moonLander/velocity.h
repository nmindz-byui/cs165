/*************************************************************
 * File: velocity.h
 * Author: Br. Burton
 *
 * Description: Contains the definition of the Velocity class.
 *
 * Please DO NOT share this code with other students from
 *  other sections or other semesters. They may not receive
 *  the same code that you are receiving.
 *************************************************************/

#ifndef VELOCITY_H
#define VELOCITY_H

#include "velocity.h"
#include "point.h"

/*****************************************
 * Velocity
 * The main Velocity class containing all the state
 *****************************************/
class Velocity
{
    private:
        float dx;
        float dy;
    public:
        // Constructors
        Velocity();
        // Getters
        float getDx() const;
        float getDy() const;
        // Setters
        void setDx(float _dx);
        void setDy(float _dy);
};

#endif /* VELOCITY_H */

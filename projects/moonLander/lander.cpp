/*************************************************************
 * File: lander.cpp
 * Author: Evandro Camargo
 *
 * Description: Contains the implementations of the
 *  method bodies for the Lander class.
 *
 * Please DO NOT share this code with other students from
 *  other sections or other semesters. They may not receive
 *  the same code that you are receiving.
 *************************************************************/

#include "lander.h"
#include "velocity.h"
#include "uiDraw.h"

#define DEFAULT_X_ACC	0.1		// Default X axis acceleration
#define DEFAULT_Y_ACC	0.3		// Default X axis acceleration

/******************************************
 * Lander :: Lander
 * Default Constructor
 ******************************************/
Lander :: Lander()
{
	this->alive = true; 		// The lander begins the game alive. :)
	this->landed = false; 		// And flying.
   	this->setPoint(
		Point(200.0, 200.0)
	);
	this->applyInertia(3);
	this->setVelocity(6.0, 6.0);
}

/******************************************
 * Lander :: getVelocity
 * Returns currenct velocity
 ******************************************/
Velocity Lander :: getVelocity() const
{
   return this->velocity;
}

/******************************************
 * Lander :: getAlive
 * Returns whether the lander has survived
 ******************************************/
bool Lander :: getAlive() const
{
   return this->alive;
}

/******************************************
 * Lander :: getLanded
 * Returns whether the lander landed
 ******************************************/
bool Lander :: getLanded() const
{
   return this->landed;
}

/******************************************
 * Lander :: getFuel
 * Returns the current fuel level
 ******************************************/
float Lander :: getFuel() const
{
   return this->fuel;
}

/******************************************
 * Lander :: getInertia
 * Returns the direction towards which inertia is being applied
 ******************************************/
int Lander :: getInertia() const
{
   return this->inertia;
}

/******************************************
 * Lander :: getPoint
 * Returns the current X,Y point the lander is in
 ******************************************/
Point Lander :: getPoint() const
{
   return this->point;
}

/******************************************
 * Lander :: getGravity
 * Returns the current gravity modifier
 ******************************************/
float Lander :: getGravity() const
{
   return this->gravity;
}

/******************************************
 * Lander :: setVelocity
 * Sets the lander velocity
 ******************************************/
void Lander :: setVelocity(float _dx, float _dy)
{
	this->velocity.setDx(_dx);
	this->velocity.setDy(_dy);
}

/******************************************
 * Lander :: setVelocityX
 * Sets the lander velocity
 ******************************************/
void Lander :: setVelocityX(float _dx)
{
	this->velocity.setDx(_dx);
}

/******************************************
 * Lander :: setVelocityY
 * Sets the lander velocity
 ******************************************/
void Lander :: setVelocityY(float _dy)
{
	this->velocity.setDy(_dy);
}

/******************************************
 * Lander :: setAlive
 * Sets the alive flag
 ******************************************/
void Lander :: setAlive(bool _alive)
{
   this->alive = _alive;
}

/******************************************
 * Lander :: setLanded
 * Sets the landed flag
 ******************************************/
void Lander :: setLanded(bool _landed)
{
   this->landed = _landed;
}

/******************************************
 * Lander :: setFuel
 * Sets the current fuel level
 ******************************************/
void Lander :: setFuel(float _fuel)
{
   this->fuel = _fuel;
}

/******************************************
 * Lander :: setInertia
 * Sets the inertia direction
 ******************************************/
void Lander :: setInertia(int _inertia)
{
   this->inertia = _inertia;
}

/******************************************
 * Lander :: setPoint
 * Sets the current X,Y point in space
 ******************************************/
void Lander :: setPoint(Point _point)
{
   this->point.setX(_point.getX());
   this->point.setY(_point.getY());
}

/******************************************
 * Lander :: setGravity
 * Applies a new gravity modifier
 ******************************************/
void Lander :: setGravity(float _gravity)
{
	if (_gravity > 0)
	{
		this->gravity = _gravity;
	}
	else
	{
		this->gravity = 0.1;
	}
}

/******************************************
 * Lander :: applyGravity
 * Sets the applied gravity
 ******************************************/
void Lander :: applyGravity(float _gravity)
{
	this->setGravity(_gravity);
}

/******************************************
 * Lander :: applyThrustLeft
 * Sets thrusters to left (Apply Inertia 1)
 ******************************************/
void Lander :: applyThrustLeft()
{
	if (this->canThrust())
	{
		drawLanderFlames(this->getPoint(), false, true, false);

		this->setVelocityX(
			(this->getVelocity().getDx() + DEFAULT_X_ACC)
		);
		this->applyInertia(1); // 1 = Left Thrust

		this->setFuel(
			this->getFuel() - 1
		);
	}
}

/******************************************
 * Lander :: applyThrustRight
 * Sets thrusters to right (Apply Inertia 2)
 ******************************************/
void Lander :: applyThrustRight()
{
   if (this->canThrust())
	{
		drawLanderFlames(this->getPoint(), false, false, true);
		
		this->setVelocityX(
			(this->getVelocity().getDx() + DEFAULT_X_ACC)
		);
		this->applyInertia(2); // 2 = Right Thrust

		this->setFuel(
			this->getFuel() - 1
		);
	}
}

/******************************************
 * Lander :: applyThrustBottom
 * Sets thrusters to bottom (Apply Inertia 0)
 ******************************************/
void Lander :: applyThrustBottom()
{
	if (this->canThrust())
	{
		drawLanderFlames(this->getPoint(), true, false, false);

		this->setVelocityY(
			(this->getVelocity().getDy() - 0.8)
		);
		this->applyInertia(0); // 0 = Bottom Thrust

		this->setFuel(
			this->getFuel() - 3
		);
	}
}

/******************************************
 * Lander :: applyInertia
 * Applies inertia effect
 ******************************************/
void Lander :: applyInertia(int _inertia)
{
	this->setInertia(_inertia);

	switch (this->getInertia())
	{
		case 0:
			this->point.addY(-0.3);
			// this->point.addX(0.1);
			break;
		case 1:
			this->point.addX(0.2 * this->getVelocity().getDx());
			break;
		case 2:
			this->point.addX(-0.1 * this->getVelocity().getDx());
			break;
		default:		
			break;
	}

	this->point.addX(-0.1 * this->getVelocity().getDx());
	this->point.addY(-0.1 * this->getVelocity().getDy());
}

/******************************************
 * Lander :: isAlive
 * Returns whether alive or not
 ******************************************/
bool Lander :: isAlive()
{
   return this->getAlive();
}

/******************************************
 * Lander :: isLanded
 * Returns whether landed or not
 ******************************************/
bool Lander :: isLanded()
{
   return this->getLanded();
}

/******************************************
 * Lander :: canThrust
 * Returns whether it can use thrusters or not
 ******************************************/
bool Lander :: canThrust()
{
   return this->getFuel() > 0;
}

/******************************************
 * Lander :: advance
 * Moves forward
 ******************************************/
void Lander :: advance()
{
   this->setPoint(
		Point(this->getPoint().getX(), this->getPoint().getY() - this->getGravity())
	);

	this->applyInertia(this->getInertia()); // Default inertia

   if (this->getVelocity().getDx() > 2.9)
	{
		// Slowly decreases X axis acceleration, down to landing speeds
		this->setVelocity(this->getVelocity().getDx() - 0.05, this->getVelocity().getDy());
	}

	// Maintains a steady fall speed, that can be countered by bottom thrusters
	if (this->getVelocity().getDy() < 6.0)
	{
		this->setVelocityY(this->getVelocity().getDy() + this->getGravity());
	}
}

/******************************************
 * Lander :: draw
 * Draws the lander at each given point
 ******************************************/
void Lander :: draw()
{
   drawLander(this->getPoint());
}
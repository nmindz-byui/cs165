/*************************************************************
 * File: velocity.cpp
 * Author: Evandro Camargo
 *
 * Description: Contains the implementations of the
 *  method bodies for the Velocity class.
 *
 * Please DO NOT share this code with other students from
 *  other sections or other semesters. They may not receive
 *  the same code that you are receiving.
 *************************************************************/

#include "velocity.h"

/******************************************
 * Velocity :: Velocity
 * Default Constructor
 ******************************************/
Velocity :: Velocity(float _dx, float _dy)
{
   this->setDx(_dx);
   this->setDy(_dy);
}

/******************************************
 * Velocity :: Velocity
 * Gets the X acceleration
 ******************************************/
float Velocity :: getDx() const
{
   return dx;
}

/******************************************
 * Velocity :: Velocity
 * Gets the Y acceleration
 ******************************************/
float Velocity :: getDy() const
{
   return dy;
}

/******************************************
 * Velocity :: Velocity
 * Sets the X acceleration
 ******************************************/
void Velocity :: setDx(float _dx)
{
   if (_dx > 0)
   {
      this->dx = _dx;
   }
   else
   {
      this->dx = 0;
   }
}

/******************************************
 * Velocity :: Velocity
 * Sets the Y acceleration
 ******************************************/
void Velocity :: setDy(float _dy)
{
   this->dy = _dy;
}

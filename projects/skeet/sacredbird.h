/*************************************************************
 * File: sacredbird.h
 * Author: Evandro Camargo
 *
 * Description: Defines a SacredBird.
 *
 *************************************************************/

#ifndef SACREDBIRD_H
#define SACREDBIRD_H

#include "bird.h"

class SacredBird : public Bird
{
   private:
   public:
      ~SacredBird() {};
      SacredBird()
      {
         this->setName("Sacred Bird");
         this->setHitpoints(1);
         this->setReward(0);
         this->setBonus(0);
         this->setPenalty(10);
         this->setSpeed(Velocity(random(3,6), random(1,4)));
      };
      void draw()
      {
         drawSacredBird(this->getPoint(), 15);
      };
};

#endif // SACREDBIRD_H

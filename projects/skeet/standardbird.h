/*************************************************************
 * File: standardbird.h
 * Author: Evandro Camargo
 *
 * Description: Defines a StandardBird.
 *
 *************************************************************/

#ifndef STANDARDBIRD_H
#define STANDARDBIRD_H

#include "bird.h"

class StandardBird : public Bird
{
   private:
   public:
      ~StandardBird() {};
      StandardBird()
      {
         this->setName("Standard Bird");
         this->setHitpoints(1);
         this->setReward(1);
         this->setBonus(0);
         this->setPenalty(0);
         this->setSpeed(Velocity(random(3,6), random(1,4)));
      };
      void draw()
      {
         drawCircle(this->getPoint(), 15);
      };
};

#endif // STANDARDBIRD_H

/*************************************************************
 * File: bullet.h
 * Author: Evandro Camargo
 *
 * Description: Defines a Bullet.
 *
 *************************************************************/

#ifndef BULLET_H
#define BULLET_H

#include "point.h"
#include "velocity.h"

class Bullet
{
   private:
      Point point;
      bool alive;
      Velocity speed;
   public:
      Bullet();
      // Getters
      Point getPoint() const;
      Velocity getSpeed() const;
      bool getAlive() const;
      // Setters
      void setPoint(const Point _point);
      void setSpeed(const Velocity _speed);
      void setAlive(const bool _alive);
      // Control
      bool isAlive();
      // Triggers
      void kill();
      void advance();
      void draw();
      void fire(Point _point, float _angle);
};

#endif // BULLET_H

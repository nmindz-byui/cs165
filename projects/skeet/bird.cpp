/*************************************************************
 * File: bird.cpp
 * Author: Evandro Camargo
 *
 * Description: Contains the function bodies for the Bird class.
 *
 *************************************************************/

#include "bird.h"
#include "point.h"
#include "uiDraw.h"

#include <cassert>

/******************************************
 * Bird :: getPoint
 * 
 ******************************************/
Point Bird :: getPoint() const
{
   return this->point;
}

/******************************************
 * Bird :: getAlive
 * 
 ******************************************/
bool Bird :: getAlive() const
{
   return this->alive;
}

/******************************************
 * Bird :: getName
 * 
 ******************************************/
std::string Bird :: getName() const
{
   return this->name;
}

/******************************************
 * Bird :: getReward
 * 
 ******************************************/
int Bird :: getReward() const
{
   return this->reward;
}

/******************************************
 * Bird :: getBonus
 * 
 ******************************************/
int Bird :: getBonus() const
{
   return this->bonus;
}

/******************************************
 * Bird :: getPenalty
 * 
 ******************************************/
int Bird :: getPenalty() const
{
   return this->penalty;
}

/******************************************
 * Bird :: getHitpoints
 * 
 ******************************************/
int Bird :: getHitpoints() const
{
   return this->hitpoints;
}

/******************************************
 * Bird :: getSpeed
 * 
 ******************************************/
Velocity Bird :: getSpeed() const
{
   return this->speed;
}

/******************************************
 * Bird :: getAngle
 * 
 ******************************************/
int Bird :: getAngle() const
{
   return this->angle;
}

/******************************************
 * Bird :: setPoint
 * 
 ******************************************/
void Bird :: setPoint(const Point _point)
{
   this->point = _point;
}

/******************************************
 * Bird :: setAlive
 * 
 ******************************************/
void Bird :: setAlive(const bool _alive)
{
   this->alive = _alive;
}

/******************************************
 * Bird :: setName
 * 
 ******************************************/
void Bird :: setName(const std::string _name)
{
   this->name = _name;
}

/******************************************
 * Bird :: setReward
 * 
 ******************************************/
void Bird :: setReward(const int _reward)
{
   this->reward = _reward;
}

/******************************************
 * Bird :: setBonus
 * 
 ******************************************/
void Bird :: setBonus(const int _bonus)
{
   this->bonus = _bonus;
}

/******************************************
 * Bird :: setPenalty
 * 
 ******************************************/
void Bird :: setPenalty(const int _penalty)
{
   this->penalty = _penalty;
}

/******************************************
 * Bird :: setHitpoints
 * 
 ******************************************/
void Bird :: setHitpoints(const int _hitpoints)
{
   this->hitpoints = _hitpoints;
}

/******************************************
 * Bird :: setHSpeed
 * 
 ******************************************/
void Bird :: setSpeed(const Velocity _speed)
{
   this->speed = _speed;
}


/******************************************
 * Bird :: isAlive
 * 
 ******************************************/
bool Bird :: isAlive()
{
   if (this->getHitpoints() > 0)
   {
      this->setAlive(true);
   }
   else
   {
      this->setAlive(false);
   }

   return this->getAlive();
}

/******************************************
 * Bird :: kill
 * 
 ******************************************/
void Bird :: kill()
{
   this->setHitpoints(0);
}

/******************************************
 * Bird :: advance
 * 
 ******************************************/
void Bird :: advance()
{
   Point _p = this->getPoint();

   _p.addX(this->getSpeed().getDx());

   if (this->getAngle() > 0)
   {
      _p.addY(-this->getSpeed().getDy());
   }
   else
   {
      _p.addY(this->getSpeed().getDy());
   }

   this->setPoint(_p);
}

/******************************************
 * Bird :: draw
 * 
 ******************************************/
void Bird :: draw()
{
   //
}

/******************************************
 * Bird :: hit
 * 
 ******************************************/
int Bird :: hit()
{
   this->setHitpoints(this->getHitpoints() - 1);
   if (this->getBonus() > 0 && this->getHitpoints() == 0)
   {
      return (this->getReward() + this->getBonus() - this->getPenalty());
   }
   return (this->getReward() - this->getPenalty());
}
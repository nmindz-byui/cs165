/*************************************************************
 * File: bird.h
 * Author: Evandro Camargo
 *
 * Description: Defines a Bird.
 *
 *************************************************************/

#ifndef BIRD_H
#define BIRD_H

#include <string>
#include "point.h"
#include "velocity.h"
#include "uiDraw.h"

class Bird
{
   private:
      Point point;
      bool alive;
      int hitpoints;
      std::string name;
      int reward;
      int bonus;
      int penalty;
      Velocity speed;
      int angle;
   public:
      Bird() {
         this->setPoint(Point(-200, random(-200, 200)));
         this->angle = this->getPoint().getY();
      };
      ~Bird() {};
      // Getters
      Point getPoint() const;
      bool getAlive() const;
      std::string getName() const;
      int getReward() const;
      int getBonus() const;
      int getPenalty() const;
      int getHitpoints() const;
      Velocity getSpeed() const;
      int getAngle() const;
      // Setters
      void setPoint(const Point _point);
      void setAlive(const bool _alive);
      void setName(const std::string _name);
      void setReward(const int _reward);
      void setBonus(const int _bonus);
      void setPenalty(const int _penalty);
      void setHitpoints(const int _hitpoints);
      void setSpeed(const Velocity _speed);
      // Control
      bool isAlive();
      // Triggers
      void kill();
      void advance();
      virtual void draw();
      int hit();
};

#endif // BIRD_H

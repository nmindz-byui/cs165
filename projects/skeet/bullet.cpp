/*************************************************************
 * File: bullet.cpp
 * Author: Evandro Camargo
 *
 * Description: Contains the function bodies for the Bullet class.
 *
 *************************************************************/

#include "velocity.h"
#include "bullet.h"
#include "point.h"
#include "uiDraw.h"
#include <cmath>  // used for sin, cos, and M_PI ...

#define BULLET_SPEED 10.0


/******************************************
 * Bullet :: Bullet
 * 
 ******************************************/
Bullet :: Bullet()
{
   this->setAlive(true);
}

/******************************************
 * Bullet :: Bullet
 * 
 ******************************************/
Point Bullet :: getPoint() const
{
   return this->point;
}

/******************************************
 * Bullet :: Bullet
 * 
 ******************************************/
Velocity Bullet :: getSpeed() const
{
   return this->speed;
}

/******************************************
 * Bullet :: Bullet
 * 
 ******************************************/
bool Bullet :: getAlive() const
{
   return this->alive;
}

/******************************************
 * Bullet :: Bullet
 * 
 ******************************************/
void Bullet :: setPoint(const Point _point)
{
   this->point = _point;
}

/******************************************
 * Bullet :: Bullet
 * 
 ******************************************/
void Bullet :: setSpeed(const Velocity _speed)
{
   this->speed = _speed;
}

/******************************************
 * Bullet :: Bullet
 * 
 ******************************************/
void Bullet :: setAlive(const bool _alive)
{
   this->alive = _alive;
}

/******************************************
 * Bullet :: Bullet
 * 
 ******************************************/
bool Bullet :: isAlive()
{
   return this->getAlive();
}

/******************************************
 * Bullet :: Bullet
 * 
 ******************************************/
void Bullet :: kill()
{
   this->setAlive(false);
}

/******************************************
 * Bullet :: Bullet
 * 
 ******************************************/
void Bullet :: advance()
{
   Point _p = this->getPoint();

   _p.addX(-this->getSpeed().getDx());
   _p.addY(this->getSpeed().getDy());

   this->setPoint(_p);
}

/******************************************
 * Bullet :: Bullet
 * 
 ******************************************/
void Bullet :: draw()
{
   drawDot(this->getPoint());
}

/******************************************
 * Bullet :: Bullet
 * 
 ******************************************/
void Bullet :: fire(Point _point, float _angle)
{
   // float dx = BULLET_SPEED * (-cos(M_PI / 180.0 * _angle));
   float dx = BULLET_SPEED * (cos(M_PI / 180.0 * _angle));
   float dy = BULLET_SPEED * (sin(M_PI / 180.0 * _angle));

   Velocity _speed = Velocity(dx, dy);
   this->setSpeed(_speed);
   this->setPoint(_point);

   drawDot(this->getPoint());
}

/*************************************************************
 * File: toughbird.h
 * Author: Evandro Camargo
 *
 * Description: Defines a ToughBird.
 *
 *************************************************************/

#ifndef TOUGHBIRD_H
#define TOUGHBIRD_H

#include "bird.h"

class ToughBird : public Bird
{
   private:
   public:
      ~ToughBird() {};
      ToughBird()
      {
         this->setName("Tough Bird");
         this->setHitpoints(3);
         this->setReward(1);
         this->setBonus(2);
         this->setPenalty(0);
         this->setSpeed(Velocity(random(2,4), random(1,3)));
      };
      void draw()
      {
         drawToughBird(this->getPoint(), 15, 3);
      };
};

#endif // TOUGHBIRD_H

/*************************************************************
 * File: bullet.cpp
 * Author: Evandro Camargo
 *
 * Description: Implements the Bullet class
 *
 *************************************************************/

#include "headers/bullet.h"
#include "headers/uiDraw.h"

/******************************************
 * Bullet :: Bullet
 * Default Constructor
 ******************************************/
Bullet :: Bullet()
{
   this->setHitpoints(BULLET_LIFE);
   this->setAlive(true);
}

/******************************************
 * Bullet :: ~Bullet
 * Default Destructor
 ******************************************/
Bullet :: ~Bullet()
{
   //
}

/******************************************
 * Bullet :: getHitpoints
 * Returns the Bullet hitpoints
 ******************************************/
int Bullet :: getHitpoints() const
{
   return this->hitpoints;
}

/******************************************
 * Bullet :: setHitpoints
 * Sets the Bullet hitpoints
 ******************************************/
void Bullet :: setHitpoints(int _hitpoints)
{
   // if (this->getHitpoints() > 0)
   // {
      this->hitpoints = _hitpoints;
   // }
}

/******************************************
 * Bullet :: advance
 * Advances one frame at a time
 ******************************************/
void Bullet :: advance()
{
   if (this->getHitpoints() < 1)
   {
      this->setAlive(false);
      return;
   }

   // Decreases bullet's "frame-span" by 1
   this->setHitpoints(this->getHitpoints() - 1);

   float _dx = this->getSpeed().getDx();
   float _dy = this->getSpeed().getDy();

   Point _p = this->getPosition();

   _p.addX(_dx);
   _p.addY(_dy);

   this->setPosition(_p);
}

/******************************************
 * Bullet :: draw
 * Draws the Bullet
 ******************************************/
void Bullet :: draw() const
{
   drawDot(this->getPosition());
}

/******************************************
 * Bullet :: hit
 * Hits the Bullet
 ******************************************/
void Bullet :: hit()
{
   this->setHitpoints(0);
   this->setAlive(false);
}

/******************************************
 * Bullet :: fire
 * Handles the Bullet being fired
 ******************************************/
void Bullet :: fire(Point _point, float _angle, Velocity _shipVelocity)
{
   Velocity _bulletSpeed(BULLET_SPEED, BULLET_SPEED);
   _bulletSpeed += _shipVelocity;

   _bulletSpeed.setDx(_bulletSpeed.getDx() * (-sin(RADIANS_CONST * _angle)));
   _bulletSpeed.setDy(_bulletSpeed.getDy() * (cos(RADIANS_CONST * _angle)));

   this->setSpeed(_bulletSpeed);
   this->setPosition(_point);

   drawDot(this->getPosition());
}
#ifndef ASTEROIDS_H
#define ASTEROIDS_H

#define LARGE_ASTEROID_SIZE 16
#define MEDIUM_ASTEROID_SIZE 8
#define SMALL_ASTEROID_SIZE 4

#define LARGE_ASTEROID_SPIN 2
#define MEDIUM_ASTEROID_SPIN 5
#define SMALL_ASTEROID_SPIN 10

#include "flyingObject.h"
#include "vector"

/*****************************************
 * Asteroid
 * The Asteroid class definition
 *****************************************/
class Asteroid : public FlyingObject
{
   private:
      int radius;
   public:
      Asteroid()           {};
      virtual ~Asteroid()  {};
      // Getters
      int getRadius() const;
      // Setters
      void setRadius(int _radius);
      // Triggers
      void advance();
      void hit();
      virtual void draw() const = 0;
      virtual std::vector<Asteroid*> split() const = 0;
      virtual void rotate() = 0;
};

#endif /* ASTEROIDS_H */

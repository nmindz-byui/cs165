/*********************************************************************
 * File: game.h
 * Description: Defines the game class for Asteroids
 *
 *********************************************************************/

#ifndef GAME_H
#define GAME_H

#include "uiDraw.h"
#include "uiInteract.h"
#include "point.h"
#include "velocity.h"
#include "bullet.h"
#include "asteroid.h"
#include "ship.h"
#include <vector>

#define CLOSE_ENOUGH 15

class Game
{
   private:   
      Point topLeft;
      Point bottomRight;
      std::vector<Bullet*> bullets;
      std::vector<Asteroid*> asteroids;
      Ship* ship;
      Asteroid* pAsteroid;
      void advanceBullets();
      void advanceAsteroids();
      void advanceShip();
      void splitAsteroid(Asteroid* _asteroid);
      void createLargeAsteroid();
      void handleCollisions();
      void cleanUpZombies();
   public:
      Game(Point tl, Point br);
      ~Game();
      void handleInput(const Interface & ui);
      void advance();
      void draw(const Interface & ui);
      float getClosestDistance(const FlyingObject &obj1, const FlyingObject &obj2) const;
};


#endif /* GAME_H */

#ifndef MEDIUMASTEROID_H
#define MEDIUMASTEROID_H

#include "asteroid.h"
#include "uiDraw.h"

#define M_INITIAL_ANGLE random(0, 180)
#define M_ASTEROID_RADIUS 8
#define M_ASTEROID_R_SPEED Velocity(2.0, 2.0)
#define M_ASTEROID_ROTATE 5
#define M_ASTEROID_SPAWN_COUNT 2

class MediumAsteroid : public Asteroid
{
   private:
   public:
      MediumAsteroid(Point position, Velocity _speed)
      {
         this->setRadius(M_ASTEROID_RADIUS);
         this->setPosition(position);
         this->setAngle(M_INITIAL_ANGLE);
         this->setAngSpeed(M_INITIAL_ANGLE, _speed + M_ASTEROID_R_SPEED);
      }
      ~MediumAsteroid() {};
      void draw() const
      {
         drawMediumAsteroid(this->getPosition(), this->getAngle());
      }
      std::vector<Asteroid*> split() const
      {
         std::vector<Asteroid*> _smallAsteroids;
         int i = 0;

         while (i < M_ASTEROID_SPAWN_COUNT)
         {
            SmallAsteroid* pAsteroid = new SmallAsteroid(this->getPosition(), this->getSpeed());
            _smallAsteroids.push_back(pAsteroid);
            i++;
         }

         return _smallAsteroids;
      }
      void rotate()
      {
         this->setAngle(this->getAngle() + M_ASTEROID_ROTATE);
      }
};

#endif /* MEDIUMASTEROID_H */

#ifndef FLYINGOBJECT_H
#define FLYINGOBJECT_H

#include "point.h"
#include "velocity.h"
#include <cmath>

#define RADIANS_CONST (M_PI / 180.0)

class FlyingObject
{
   private:
      Point position;
      float angle;
      Velocity speed;
      bool alive;
   public:
      // Constructor
      FlyingObject() :  alive(true) {};
      virtual ~FlyingObject()       {};
      // Getters
      Point getPosition() const;
      float getAngle() const;
      Velocity getSpeed() const;
      bool getAlive() const;
      // Setters
      void setPosition(Point _position);
      void setAngle(float _angle);
      void setSpeed(Velocity _speed);
      void setAngSpeed(float _angle, Velocity _speed);
      void setAlive(bool _alive);
      // Misc
      bool isAlive() { return this->getAlive(); }
      virtual void rotate() = 0;
      virtual void draw() const = 0;
      virtual void hit() = 0;
};

#endif /* FLYINGOBJECT_H */

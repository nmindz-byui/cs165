#ifndef LARGEASTEROID_H
#define LARGEASTEROID_H

#include "asteroid.h"
#include "uiDraw.h"

#define L_INITIAL_ANGLE random(0, 180)
#define L_ASTEROID_RADIUS 16
#define L_ASTEROID_SPEED Velocity(1.0, 1.0) // BASE SPEED
#define L_ASTEROID_ROTATE 2
#define L_ASTEROID_SPAWN_COUNT 2

class LargeAsteroid : public Asteroid
{
   private:
   public:
      LargeAsteroid(Point position)
      {
         this->setRadius(L_ASTEROID_RADIUS);
         this->setPosition(position);
         this->setAngle(L_INITIAL_ANGLE);
         this->setAngSpeed(L_INITIAL_ANGLE, L_ASTEROID_SPEED);
      }
      ~LargeAsteroid() {};
      void draw() const
      {
         drawLargeAsteroid(this->getPosition(), this->getAngle());
      }
      std::vector<Asteroid*> split() const
      {
         std::vector<Asteroid*> _mediumAsteroids;
         int i = 0;

         while (i < L_ASTEROID_SPAWN_COUNT)
         {
            MediumAsteroid* pAsteroid = new MediumAsteroid(this->getPosition(), this->getSpeed());
            _mediumAsteroids.push_back(pAsteroid);
            i++;
         }

         return _mediumAsteroids;
      }
      void rotate()
      {
         this->setAngle(this->getAngle() + L_ASTEROID_ROTATE);
      }
};

#endif /* LARGEASTEROID_H */

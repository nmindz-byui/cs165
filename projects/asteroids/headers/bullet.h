#ifndef BULLET_H
#define BULLET_H

#define BULLET_SPEED 5
#define BULLET_LIFE 40

#include "flyingObject.h"

/*****************************************
 * Bullet
 * The Bullet class definition
 *****************************************/
class Bullet : public FlyingObject
{
   private:
      int hitpoints;
   public:
      // Constructors
      Bullet();
      ~Bullet();
      // Getters
      int getHitpoints() const;
      // Setters
      void setHitpoints(int _hitpoints);
      // Triggers
      void advance();
      void rotate() {}; // Since the bullet is just a dot, it should not rotate.
      void draw() const;
      void hit();
      void fire(Point _point, float _angle, Velocity _shipVelocity);
};

#endif /* BULLET_H */

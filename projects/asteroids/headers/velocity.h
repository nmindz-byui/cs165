#ifndef VELOCITY_H
#define VELOCITY_H

#include "velocity.h"
#include "point.h"

/*****************************************
 * Velocity
 * The main Velocity class containing all the state
 *****************************************/
class Velocity
{
    private:
        float dx;
        float dy;
    public:
        // Constructors
        Velocity() {};
        Velocity(const Velocity&);      // Copy constructor
        Velocity(Velocity&&) = default; // Move constructor
        Velocity(float _dx, float _dy);
        // Destructor
        virtual ~Velocity() {};
        // Getters
        float getDx() const;
        float getDy() const;
        // Setters
        void setDx(float _dx);
        void setDy(float _dy);
        // Operator Overloading
        Velocity& operator=(const Velocity&);           // Copy assignment operator
        Velocity& operator=(Velocity&&) = default;      // Move assignment operator
        Velocity& operator+=(const Velocity&);          // Add onto operator
        Velocity operator+(const Velocity& rhs) const;  // Add operator
};

#endif /* VELOCITY_H */
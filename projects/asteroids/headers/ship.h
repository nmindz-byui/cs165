#ifndef SHIP_H
#define SHIP_H

#define SHIP_SIZE 10
#define ROTATE_AMOUNT 6
#define THRUST_AMOUNT 0.5
#define MINIMUM_ANGLE 6.0
#define MAXIMUM_ANGLE 354.0
#define ANGLE_ZERO 0.0
#define ANGLE_FULL 360.0

#include "flyingObject.h"

class Ship : public FlyingObject
{
   private:
      int radius;
      bool isMoving;
      bool isThrusting;
   public:
      // Constructors
      Ship(const Point & _position, float _angle);
      ~Ship();
      // Getters
      int getRadius() const;
      bool getIsMoving() const;
      bool getIsThrusting() const;
      // Setters
      void setRadius(int _radius);
      void setIsMoving(bool _isMoving);
      void setIsThrusting(bool _isThrusting);
      // Controls
      void rotateLeft();
      void rotateRight();
      void thrust();
      // Triggers
      void advance();
      void rotate() {}; // The ship does not rotate by inertia, only by user control
      void draw() const;
      void hit();
};


#endif /* SHIP_H */

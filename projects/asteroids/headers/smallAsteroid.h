#ifndef SMALLASTEROID_H
#define SMALLASTEROID_H

#include "asteroid.h"
#include "uiDraw.h"

#define S_INITIAL_ANGLE random(0, 180)
#define S_ASTEROID_RADIUS 4
#define S_ASTEROID_R_SPEED Velocity(3.0, 3.0)
#define S_ASTEROID_ROTATE 10

class SmallAsteroid : public Asteroid
{
   private:
   public:
      SmallAsteroid(Point position, Velocity _speed)
      {
         this->setRadius(S_ASTEROID_RADIUS);
         this->setPosition(position);
         this->setAngle(S_INITIAL_ANGLE);
         this->setAngSpeed(S_INITIAL_ANGLE, _speed + S_ASTEROID_R_SPEED);
      }
      ~SmallAsteroid() {};
      void draw() const
      {
         drawSmallAsteroid(this->getPosition(), this->getAngle());
      }
      std::vector<Asteroid*> split() const
      {
         std::vector<Asteroid*> _noAsteroids;
         return _noAsteroids;
      }
      void rotate()
      {
         this->setAngle(this->getAngle() + S_ASTEROID_ROTATE);
      }
};

#endif /* SMALLASTEROID_H */

/*************************************************************
 * File: ship.cpp
 * Author: Evandro Camargo
 *
 * Description: Implements the Ship class
 *
 *************************************************************/

#include "headers/ship.h"
#include "headers/uiDraw.h"

/******************************************
 * Ship :: Ship
 * Default Constructor
 ******************************************/
Ship :: Ship(const Point & _position, float _angle)
{
   this->setPosition(_position);
   this->setAngle(_angle);
   this->setIsMoving(false);
   this->setRadius(SHIP_SIZE);
}

/******************************************
 * Ship :: ~Ship
 * Default Destructor
 ******************************************/
Ship :: ~Ship()
{
   //
}

/******************************************
 * Ship :: getRadius
 * Returns the Ship radius
 ******************************************/
int Ship :: getRadius() const
{
   return this->radius;
}

/******************************************
 * Ship :: getIsMoving
 * Returns the Ship movement
 ******************************************/
bool Ship :: getIsMoving() const
{
   return this->isMoving;
}

/******************************************
 * Ship :: getIsThrusting
 * Returns the Ship thrust effect
 ******************************************/
bool Ship :: getIsThrusting() const
{
   return this->isThrusting;
}


/******************************************
 * Ship :: setRadius
 * Sets the Ship radius
 ******************************************/
void Ship :: setRadius(int _radius)
{
   this->radius = _radius;
}

/******************************************
 * Ship :: setIsMoving
 * Sets the Ship movement
 ******************************************/
void Ship :: setIsMoving(bool _isMoving)
{
   this->isMoving = _isMoving;
}

/******************************************
 * Ship :: setIsThrusting
 * Sets the Ship thrust effect
 ******************************************/
void Ship :: setIsThrusting(bool _isThrusting)
{
   this->isThrusting = _isThrusting;
}

/******************************************
 * Ship :: rotateLeft
 * Rotates the Ship left
 ******************************************/
void Ship :: rotateLeft()
{
   if (this->getAngle() > MAXIMUM_ANGLE)
   {
      this->setAngle(ANGLE_ZERO);
   }
   this->setAngle(this->getAngle() + ROTATE_AMOUNT);
}

/******************************************
 * Ship :: rotateRight
 * Rotates the Ship right
 ******************************************/
void Ship :: rotateRight()
{
   if (this->getAngle() < MINIMUM_ANGLE)
   {
      this->setAngle(ANGLE_FULL);
   }
   this->setAngle(this->getAngle() - ROTATE_AMOUNT);
}

/******************************************
 * Ship :: thrust
 * Advances one frame at a time
 ******************************************/
void Ship :: thrust()
{
   float _rads = this->getAngle() * RADIANS_CONST;
   Velocity _movement(THRUST_AMOUNT*(-sin(_rads)), THRUST_AMOUNT*(cos(_rads)));
   Velocity _newSpeed = (this->getSpeed() + _movement);
   
   this->setIsThrusting(true);
   this->setSpeed(_newSpeed);
}

/******************************************
 * Ship :: advance
 * Advances one frame at a time
 ******************************************/
void Ship :: advance()
{
   Point _p = this->getPosition();

   _p.addX(this->getSpeed().getDx());
   _p.addY(this->getSpeed().getDy());
   
   this->setPosition(_p);
   this->setIsThrusting(false);
}

/******************************************
 * Ship :: draw
 * Draws the Ship
 ******************************************/
void Ship :: draw() const
{
   if (this->getAlive())
   {
      drawShip(this->getPosition(), this->getAngle(), this->getIsThrusting());
   }
}

/******************************************
 * Ship :: draw
 * Handles the Ship being hit
 ******************************************/
void Ship :: hit()
{
   this->setAlive(false);
}
/*************************************************************
 * File: flyingObject.cpp
 * Author: Evandro Camargo
 *
 * Description: Implements the FlyingObject class
 *
 *************************************************************/

#include "headers/flyingObject.h"

#define MAXIMUM_PIXEL_WRAP 200
#define MINIMUM_PIXEL_WRAP -200

/******************************************
 * FlyingObject :: getPosition
 * Returns the FlyingObject position
 ******************************************/
Point FlyingObject :: getPosition() const
{
   return this->position;
}

/******************************************
 * FlyingObject :: getAngle
 * Returns the FlyingObject angle
 ******************************************/
float FlyingObject :: getAngle() const
{
   return this->angle;
}

/******************************************
 * FlyingObject :: getSpeed
 * Returns the FlyingObject speed
 ******************************************/
Velocity FlyingObject :: getSpeed() const
{
   return this->speed;
}

/******************************************
 * FlyingObject :: getAlive
 * Returns the FlyingObject alive attribute
 ******************************************/
bool FlyingObject :: getAlive() const
{
   return this->alive;
}

/******************************************
 * FlyingObject :: setPosition
 * Sets the FlyingObject position
 ******************************************/
void FlyingObject :: setPosition(Point _position)
{
   if (_position.getX() > MAXIMUM_PIXEL_WRAP)
   {
      this->position.setX(MINIMUM_PIXEL_WRAP);
   }
   else if (_position.getY() > MAXIMUM_PIXEL_WRAP)
   {
      this->position.setY(MINIMUM_PIXEL_WRAP);
   }
   else if (_position.getX() < MINIMUM_PIXEL_WRAP)
   {
      this->position.setX(MAXIMUM_PIXEL_WRAP);
   }
   else if (_position.getY() < MINIMUM_PIXEL_WRAP)
   {
      this->position.setY(MAXIMUM_PIXEL_WRAP);
   }
   else
   {
      this->position = _position;
   }
}

/******************************************
 * FlyingObject :: setAngle
 * Sets the FlyingObject angle
 ******************************************/
void FlyingObject :: setAngle(float _angle)
{
   this->angle = _angle;
}

/******************************************
 * FlyingObject :: setSpeed
 * Sets the FlyingObject speed
 ******************************************/
void FlyingObject :: setSpeed(Velocity _speed)
{
   this->speed = _speed;
}

/******************************************
 * FlyingObject :: setAngSpeed
 * Sets the FlyingObject angular speed
 ******************************************/
void FlyingObject :: setAngSpeed(float _angle, Velocity _speed)
{
   float rads = _angle * RADIANS_CONST;
   float dx = _speed.getDx() * (-cos(rads));
   float dy = _speed.getDy() * (sin(rads));

   this->setSpeed(Velocity(dx, dy));
}

/******************************************
 * FlyingObject :: setAlive
 * Sets the FlyingObject alive attribute
 ******************************************/
void FlyingObject :: setAlive(bool _alive)
{
   this->alive = _alive;
}
/*************************************************************
 * File: velocity.cpp
 * Author: Evandro Camargo
 *
 * Description: Contains the implementations of the
 *  method bodies for the Velocity class.
 *
 *************************************************************/

#include "headers/velocity.h"

/******************************************
 * Velocity :: Velocity
 * Default Constructor
 ******************************************/
Velocity :: Velocity(float _dx, float _dy)
{
   this->setDx(_dx);
   this->setDy(_dy);
}

/******************************************
 * Velocity :: getDx
 * Gets the X acceleration
 ******************************************/
float Velocity :: getDx() const
{
   return dx;
}

/******************************************
 * Velocity :: getDy
 * Gets the Y acceleration
 ******************************************/
float Velocity :: getDy() const
{
   return dy;
}

/******************************************
 * Velocity :: setDx
 * Sets the X acceleration
 ******************************************/
void Velocity :: setDx(float _dx)
{
   this->dx = _dx;
}

/******************************************
 * Velocity :: setDy
 * Sets the Y acceleration
 ******************************************/
void Velocity :: setDy(float _dy)
{
   this->dy = _dy;
}

/******************************************
 * Velocity :: Velocity(const Velocity&)
 * Overloaded Copy constructor
 ******************************************/
Velocity :: Velocity(const Velocity& rhs)
{
   this->dx = rhs.dx;
   this->dy = rhs.dy;
}

/******************************************
 * Velocity :: operator=
 * Overloaded Copy assignment operator
 ******************************************/
Velocity& Velocity :: operator=(const Velocity& rhs)
{
   this->dx = rhs.dx;
   this->dy = rhs.dy;

   return *this;
}

/******************************************
 * Velocity :: operator=
 * Overloaded Copy Add onto operator
 ******************************************/
Velocity& Velocity :: operator+=(const Velocity& rhs)
{
   this->dx += rhs.dx;
   this->dy += rhs.dy;

   return *this;
}

/******************************************
 * Velocity :: operator+
 * Overloaded Add operator
 ******************************************/
Velocity Velocity :: operator+(const Velocity & rhs) const
{
   Velocity _speed(this->getDx() + rhs.getDx(), this->getDy() + rhs.getDy());
   return _speed;
}
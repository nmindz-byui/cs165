/*************************************************************
 * File: asteroid.cpp
 * Author: Evandro Camargo
 *
 * Description: Implements the Asteroid class
 *
 *************************************************************/

#include "headers/asteroid.h"

/******************************************
 * Asteroid :: getRadius
 * Returns the Asteroid radius
 ******************************************/
int Asteroid :: getRadius() const
{
   return this->radius;
}

/******************************************
 * Asteroid :: setRadius
 * Sets the Asteroid radius
 ******************************************/
void Asteroid :: setRadius(int _radius)
{
   this->radius = _radius;
}

/******************************************
 * Asteroid :: advance
 * Advances one frame at a time
 ******************************************/
void Asteroid :: advance()
{
   Point _p = this->getPosition();

   _p.addX(this->getSpeed().getDx());
   _p.addY(this->getSpeed().getDy());

   this->setPosition(_p);
   this->rotate();
}

/******************************************
 * Asteroid :: hit
 * Handles the Asteroid being hit
 ******************************************/
void Asteroid :: hit()
{
   this->setAlive(false);
}
/*************************************************************
 * File: game.cpp
 * Author: Evandro Camargo
 *
 * Description: Implements the Game class
 *
 *************************************************************/

#include "headers/game.h"
#include "headers/uiDraw.h"
#include "headers/uiInteract.h"
#include "headers/point.h"
#include "headers/smallAsteroid.h"
#include "headers/mediumAsteroid.h"
#include "headers/largeAsteroid.h"

// These are needed for the getClosestDistance function...
#include <limits>
#include <algorithm>
#include <stdlib.h>

#define OFF_SCREEN_BORDER_AMOUNT 5
#define INIT_L_ASTEROID_COUNT 5
#define INIT_SHIP_POINT Point(0,0)
#define INIT_SHIP_ANGLE 0
#define ASTEROID_SPLIT_SPAWN 2

/******************************************
 * Game :: Game(Point tl, Point br)
 * Class Constructor
 ******************************************/
Game :: Game(Point tl, Point br)
 : topLeft(tl), bottomRight(br)
{
   // Set up the initial conditions of the game
   this->ship = new Ship(INIT_SHIP_POINT, INIT_SHIP_ANGLE);
   
   for(int i = 0; i < INIT_L_ASTEROID_COUNT; i++)
   {
      this->createLargeAsteroid();
   }
}

/******************************************
 * Game :: ~Game()
 * Class Destructor
 ******************************************/
Game :: ~Game()
{
   if (this->ship != NULL)
   {
      delete this->ship;
   }

   std::vector<Asteroid*>::iterator it;
   for (it = asteroids.begin(); it != asteroids.end();)
   {
      if ((*it) != NULL)
      {
         delete *it;
         it = asteroids.erase(it);
      }
   }

   this->ship = NULL;
}

/******************************************
 * Game :: advanceBullets
 * Advances one frame at a time
 ******************************************/
void Game :: advanceBullets()
{
   std::vector<Bullet*>::iterator it;

   for (it = bullets.begin(); it != bullets.end();)
   {
      if ((*it)->isAlive() && (*it) != NULL)
      {
         (*it)->advance();
         it++;
      }
      else if ((*it) != NULL)
      {
         delete *it;
         it = bullets.erase(it);
      }
   }
}

/******************************************
 * Game :: advanceAsteroids
 * Advances one frame at a time
 ******************************************/
void Game :: advanceAsteroids()
{
   for (int i = 0; i < asteroids.size(); i++)
   {
      if (asteroids[i]->isAlive())
      {
         asteroids[i]->advance();
      }
   }
}

/******************************************
 * Game :: advanceShip
 * Advances one frame at a time
 ******************************************/
void Game :: advanceShip()
{
   // draw the ship
   if (this->ship != NULL && this->ship->getAlive())
   {
      this->ship->advance();
   }
}

/******************************************
 * Game :: createLargeAsteroids
 * Creates an Asteroid object (game init)
 ******************************************/
void Game :: createLargeAsteroid()
{
   float _x = random(-200, 200);
   float _y = random(-200, 200);

   LargeAsteroid* pAsteroid = new LargeAsteroid(Point(_x, _y));
   this->asteroids.push_back(pAsteroid);
}

/******************************************
 * Game :: splitAsteroid
 * Creates an Asteroid object
 ******************************************/
void Game :: splitAsteroid(Asteroid* _asteroid)
{
   // Placeholder local vectors for merging
   std::vector<Asteroid*> _oldAsteroids = this->asteroids;
   std::vector<Asteroid*> _newAsteroids = _asteroid->split();

   // Assures no std::length_error because of an empty/missing
   // vector in case of hitting an SmallAsteroid 
   int _oldCapacity = _oldAsteroids.capacity() || 0;
   int _newCapacity = _newAsteroids.capacity() || 0;

   // Empties the vector - prevents a segfault
   this->asteroids.clear();

   // Pre-allocate the required memory
   this->asteroids.reserve(_oldCapacity + _newCapacity);

   // Inserts the old asteroids back in place
   // and then the newly split smaller asteroids
   this->asteroids.insert(this->asteroids.end(), _oldAsteroids.begin(), _oldAsteroids.end());
   this->asteroids.insert(this->asteroids.end(), _newAsteroids.begin(), _newAsteroids.end());
}

/******************************************
 * Game :: handleCollisions
 * Handles object collision
 ******************************************/
void Game :: handleCollisions()
{
   bool _invalid_it = false;
   bool _invalid_itB = false;
   std::vector<Asteroid*>::iterator it;
   std::vector<Bullet*>::iterator itB;

   // for ship and bullet collision
   for (it = asteroids.begin(); it != asteroids.end();)
   {
      if ((*it) != NULL && (*it)->isAlive())
      {
         // check if it is close enough to the Ship
         float _distanceFromShip = this->getClosestDistance(*(*it), *this->ship);
         
         // check if any asteroid has hit the ship
         if (std::fabs(_distanceFromShip) < (*it)->getRadius())
         {
            // asteroid hit the ship
            this->ship->hit();
            
            // the asteroid isn't intact
            (*it)->hit();
         }

         // check if any living bullets are too close
         for (itB = bullets.begin(); itB != bullets.end();)
         {
            float _thisBulletDistance = this->getClosestDistance(*(*it), *(*itB));

            // check if the bullet has hit the asteroid
            if (std::fabs(_thisBulletDistance) < (*it)->getRadius())
            {
               // bullet hits the asteroid
               (*itB)->hit();
               
               // the asteroid isn't intact
               (*it)->hit();

               // Handles swapping of Asteroids
               // and makes sure everything is neat and tidy
               this->splitAsteroid(*it);

               // Makes sure the iterator will be reset since the
               // vector capacity might exceed and a new one will be alloc,
               // invalidating the current iterator
               _invalid_it = true;

               // breaks the inner loop if a hit happened
               // this ensures the invalid iterator is not used again
               break;
            }
            itB++; // moves on to the next bullet
         }
      }

      // move to the next pointer
      it++;

      // but make sure iterator is valid,
      // if not, start over.
      if (_invalid_it)
      {
         it = asteroids.begin();
         _invalid_it = false;
      }
   }
}

/******************************************
 * Game :: cleanUpZombies
 * Removes artifacts and does housekeeping
 ******************************************/
void Game :: cleanUpZombies()
{
   std::vector<Asteroid*>::iterator it;

   for (it = asteroids.begin(); it != asteroids.end();)
   {
      // check if this asteroid should be removed
      if (!(*it)->isAlive() && (*it) != NULL)
      {
         delete *it;
         it = asteroids.erase(it);
      }
      else
      {
         it++;
      }
   }
}

/******************************************
 * Game :: getPosition
 * Handles user input
 ******************************************/
void Game :: handleInput(const Interface & ui)
{
   // Change the direction of the ship
   if (ui.isLeft())
   {
      this->ship->rotateLeft();
   }
   
   if (ui.isRight())
   {
      this->ship->rotateRight();
   }
   
   if (ui.isUp())
   {
      this->ship->thrust();
   }
   
   // Check for "Spacebar
   if (ui.isSpace() && this->ship->isAlive())
   {
      Bullet *newBullet = new Bullet;
      newBullet->fire(this->ship->getPosition(), this->ship->getAngle(), this->ship->getSpeed());
      
      bullets.push_back(newBullet);
   }
}
/******************************************
 * Game :: advance
 * Advances one frame at a time
 ******************************************/
void Game :: advance()
{
   this->advanceBullets();
   this->advanceAsteroids();
   this->advanceShip();

   this->handleCollisions();
   this->cleanUpZombies();
}
/******************************************
 * Game :: draw
 * Draws the base game plane
 ******************************************/
void Game :: draw(const Interface & ui)
{
   // draw the ship
   if (this->ship != NULL)
   {
      this->ship->draw();
   }

   // draw the asteroids, if they are alive
   for (int i = 0; i < asteroids.size(); i++)
   {
      if (asteroids[i]->isAlive())
      {
         asteroids[i]->draw();
      }
   }
   
   // draw the bullets, if they are alive
   std::vector<Bullet*>::iterator bulletIt;
   for (bulletIt = bullets.begin(); bulletIt != bullets.end(); bulletIt++)
   {
      if ((*bulletIt)->isAlive())
      {
         (*bulletIt)->draw();
      }
   }
}

/**********************************************************
 * Function: getClosestDistance
 * Determine how close these two objects will get in between the frames.
 **********************************************************/
float Game :: getClosestDistance(const FlyingObject &obj1, const FlyingObject &obj2) const
{
   // find the maximum distance traveled
   float dMax = std::max(std::abs(obj1.getSpeed().getDx()), std::abs(obj1.getSpeed().getDy()));
   dMax = std::max(dMax, std::abs(obj2.getSpeed().getDx()));
   dMax = std::max(dMax, std::abs(obj2.getSpeed().getDy()));
   dMax = std::max(dMax, 0.1f); // when dx and dy are 0.0. Go through the loop once.
   
   float distMin = std::numeric_limits<float>::max();
   for (float i = 0.0; i <= dMax; i++)
   {
      Point point1(obj1.getPosition().getX() + (obj1.getSpeed().getDx() * i / dMax),
                     obj1.getPosition().getY() + (obj1.getSpeed().getDy() * i / dMax));
      Point point2(obj2.getPosition().getX() + (obj2.getSpeed().getDx() * i / dMax),
                     obj2.getPosition().getY() + (obj2.getSpeed().getDy() * i / dMax));
      
      float xDiff = point1.getX() - point2.getX();
      float yDiff = point1.getY() - point2.getY();
      
      float distSquared = (xDiff * xDiff) +(yDiff * yDiff);
      
      distMin = std::min(distMin, distSquared);
   }
   
   return sqrt(distMin);
}


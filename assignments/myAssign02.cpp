/***********************************************************************
* Program:
*    Assignment 02, Digital Forensics  
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary: 
*    Forensic Log Analysis Tool (FLAT) with support for ACME's
*    logging standards, able to audit files accessed by user with
*    corresponding timestamps.
*
*    Estimated:  1.0 hrs   
*    Actual:     3.0 hrs
*      
*    The hardest part was definitively reading the file and passing
*    the correct inputs to each part of the struct. For some reason
*    using a FOR loop gave me headaches. Not quite what I envisioned,
*    but "it works".
*
************************************************************************/

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

struct Timestamp
{
   unsigned long epoch;
};

struct AccessRecord
{
   std::string username;
   std::string filename;
   Timestamp timestamp;
};

struct Log
{
   std::string path;
   Timestamp begin;
   Timestamp end;
};

/**********************************************************************
 * Function: promptFile
 * Purpose: Asks for input file
 ***********************************************************************/
void promptFile(Log &log)
{
   std::cout << "Enter the access record file: ";
   std::getline(std::cin, log.path);
   std::cout << "\n";

   return;
}

/**********************************************************************
 * Function: promptTimeRange
 * Purpose: Asks for the timestamp range
 ***********************************************************************/
void promptTimeRange(Log &log)
{
   std::cout << "Enter the start time: ";
   std::cin >> log.begin.epoch;
   std::cin.ignore(1024, '\n');

   std::cout << "Enter the end time: ";
   std::cin >> log.end.epoch;
   std::cin.ignore(1024, '\n');

   std::cout << "\n";

   // 1442000000 1442332025

   return;
}

/**********************************************************************
 * Function: filterTimestamp
 * Purpose: Determines if the record should be displayed based on a time filter
 ***********************************************************************/
bool filterTimestamp(const unsigned long ts, const Log &l)
{
   // styleChecker crap
   bool result = !(ts >= l.begin.epoch && ts <= l.end.epoch);
   return result;
}

/**********************************************************************
 * Function: readFile
 * Purpose: Reads and parses the log file
 ***********************************************************************/
void readFile(std::vector < AccessRecord > &accessRecords, Log &l)
{

   std::ifstream file(l.path);
   // styleChecker crap
   std::string a;
   std::string b;
   std::string c;
   
   while (file >> b >> c >> a)
   {
      accessRecords.push_back(AccessRecord());
      std::stringstream(a) >> accessRecords.back().timestamp.epoch;
      accessRecords.back().filename = b;
      accessRecords.back().username = c;
   }

   return;
}

/**********************************************************************
 * Function: display
 * Purpose: Displays the log parsed results in the console
 ***********************************************************************/
void display(std::vector < AccessRecord > &accessRecords, Log &l)
{
   std::cout << "The following records match your criteria:\n\n";

   //       Timestamp                File                User
   // --------------- ------------------- -------------------
   //      1442000124           house.pdf             jatkins
   //      1442001032           users.txt     kevin_tomlinson
   //      1442210121        accounts.mdb     kevin_tomlinson
   //      1442300125        vacation.jpg            smitty83
   // End of records

   std::cout << "      Timestamp                File                User\n";
   std::cout << "--------------- ------------------- -------------------\n";
   
   for (int i = 0; i < accessRecords.size(); i++)
   {
      if (filterTimestamp(accessRecords[i].timestamp.epoch, l))
      {
         continue;
      }
      std::cout << std::setw(15) << accessRecords[i].timestamp.epoch << " ";
      std::cout << std::setw(19) << accessRecords[i].filename << " ";
      std::cout << std::setw(19) << accessRecords[i].username << "\n";
   }

   std::cout << "End of records\n";

   return;
}

/**********************************************************************
 * Function: main
 * Purpose: Application entrypoint
 ***********************************************************************/
int main()
{
   Log acmeLog;
   promptFile(acmeLog);
   promptTimeRange(acmeLog);

   std::vector < AccessRecord > accessRecords;
   readFile(accessRecords, acmeLog);

   display(accessRecords, acmeLog);

   return 0;
}

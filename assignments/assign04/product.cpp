/***************************************************************
 * File: product.cpp
 * Author: Evandro Camargo
 * Purpose: Contains the method implementations for the Product class.
 ***************************************************************/

#include <iostream>
#include "./product.h"

/**********************************************************************
 * Function: prompt
 * Purpose: Prompts the user for data
 ***********************************************************************/
void Product :: prompt()
{
   bool done = false;
   
   std::cout << "Enter name: ";
   std::getline(std::cin, name);
   std::cout << "Enter description: ";
   std::getline(std::cin, description);

   std::cout << "Enter weight: ";
   std::cin >> weight;
   
   do
   {
      std::cout << "Enter price: ";
      std::cin >> price;

      if (price < 0.00)
      {
         std::cin.setstate(std::ios_base::failbit);
      }
   
      if (std::cin.fail())
      {
         clearInputError();
      }
      else
      {
         done = true;
      }
   }
   while (!done);
}

/**********************************************************************
 * Function: getSalesTax
 * Purpose: Calculates the sale tax (6% of base price)
 ***********************************************************************/
double Product :: getSalesTax()
{
   // styleChecker does not like to return inline
   double _salesTax = price * 0.06;
   return _salesTax;
}

/**********************************************************************
 * Function: getShippingCost
 * Purpose: Calculates shipping cost based on weight
 ***********************************************************************/
double Product :: getShippingCost()
{
   double _shippingCost = 0.00;

   if (weight < 5.0)
   {
      _shippingCost = 2.00;
   }
   else
   {
      _shippingCost = ((weight - 5.00) * 0.10) + 2.00;
   }

   return _shippingCost;
}

/**********************************************************************
 * Function: getTotalPrice
 * Purpose: Returns the base price + tax and shipping fees
 ***********************************************************************/
double Product :: getTotalPrice()
{
   double _totalPrice = 0.00;

   _totalPrice = price + getSalesTax() + getShippingCost();

   return _totalPrice;
}

/**********************************************************************
 * Function: displayAdvertising
 * Purpose: Display Format 1 of the product (Advertising)
 ***********************************************************************/
void Product :: displayAdvertising()
{
   /*
      Finding Peace, Happiness, and Joy by Richard G. Scott - $14.49 (Elder Scott powerfully outlines the truths we need to understand and embrace in order to experience the gifts of peace, happiness, and joy.)
   */
   std::cout
      << name << " - $"
      << price << "\n("
      << description << ")\n";
}

/**********************************************************************
 * Function: displayInventory
 * Purpose: Display Format 2 of the product (Inventory)
 ***********************************************************************/
void Product :: displayInventory()
{
   /*
      $14.49 - Finding Peace, Happiness, and Joy by Richard G. Scott - 1.5 lbs
   */
   std::cout
      << "$" << price << " - "
      << name << " - ";

   std::cout.precision(1);

   std::cout
      << weight << " lbs\n";

   std::cout.precision(2);
}

/**********************************************************************
 * Function: displayReceipt
 * Purpose: Display Format 3 of the product (Receipt)
 ***********************************************************************/
void Product :: displayReceipt()
{
   /*
      Finding Peace, Happiness, and Joy by Richard G. Scott Price: $ 14.49 Sales tax: $ 0.87 Shipping cost: $ 2.00 Total: $ 17.36
   */
   std::cout
      << name << "\n"

      << std::setw(18)
      << "  Price:         $"
      << std::setw(8)
      << price
      << "\n"

      << std::setw(18)
      << "  Sales tax:     $"
      << std::setw(8)
      << getSalesTax()
      << "\n"

      << std::setw(18)
      << "  Shipping cost: $"
      << std::setw(8)
      << getShippingCost()
      << "\n"
      
      << std::setw(18)
      << "  Total:         $"
      << std::setw(8)
      << getTotalPrice()
      << "\n";
}

/**********************************************************************
 * Function: clearInputError
 * Purpose: Properly clears the input whenever needed
 ***********************************************************************/
void Product :: clearInputError()
{
   // std::cout << "Invalid input.\n";
   std::cin.clear();
   std::cin.ignore(4096, '\n');
}
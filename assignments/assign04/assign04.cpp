/***********************************************************************
* Program:
*    Assignment 04, Product inventory  
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary: 
*    The Product Inventory Almost-Useful Tool (tm)
*
*    Estimated:  2.0 hrs   
*    Actual:     2.0 hrs
*      
*    Since my online store took off I needed a more
*    powerful solution to handle its business. This
*    tool was then born to satisfy those needs. 
*
************************************************************************/

/***************************************************************
 * File: assign04.cpp
 * Author: Evandro Camargo
 * Purpose: Contains the main function to test the Product class.
 ***************************************************************/

#include <iomanip>
#include <iostream>
#include <string>

#include "./product.h"
#include "./product.cpp"

/**********************************************************************
 * Function: main
 * Purpose: Application entrypoint
 ***********************************************************************/
int main()
{
   std::cout.setf(std::ios::fixed);
   std::cout.setf(std::ios::showpoint);
   std::cout.precision(2);

   // Declare your Product object here
   Product myProduct;

   // Call your prompt function here
   myProduct.prompt();

   std::cout << "\n";
   std::cout << "Choose from the following options:\n";
   std::cout << "1 - Advertising profile\n";
   std::cout << "2 - Inventory line item\n";
   std::cout << "3 - Receipt\n";
   std::cout << "\n";
   std::cout << "Enter the option that you would like displayed: ";

   int choice;
   std::cin >> choice;

   std::cout << "\n";

   if (choice == 1)
   {
      myProduct.displayAdvertising();
   }
   else if (choice == 2)
   {
      myProduct.displayInventory();
   }
   else
   {
      myProduct.displayReceipt();
   }

   return 0;
}

/***************************************************************
 * File: product.h
 * Author: Evandro Camargo
 * Purpose: Contains the definition of the Product class
 ***************************************************************/
#ifndef PRODUCT_H
#define PRODUCT_H

#include <string>

/**********************************************************************
 * Class: Product
 * Purpose: Defines the structure of the "Product" class
 ***********************************************************************/
class Product
{
   private:
      std::string name;
      std::string description;
      double weight;
      double price;
      double getSalesTax();
      double getShippingCost();
      void clearInputError();
   public:
      void prompt();
      double getTotalPrice();
      void displayAdvertising();
      void displayInventory();
      void displayReceipt();
};

#endif // PRODUCT_H

/***********************************************************************
* Program:
*    Assignment 03, Digital Forensics with Corrupt Files  
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary: 
*    Forensic Log Analysis Tool (FLAT) with support for ACME's
*    logging standards, able to audit files accessed by user with
*    corresponding timestamps. Now improved!
*
*    Estimated:  1.0 hrs   
*    Actual:     1.5 hrs
*      
*    The "parseLine" part was the tricky one now. I was reading using
*    ifstream shifting, then "parseLine" was supposed to take in a
*    whole line and handle it from there (and then back to readFile).
*    I guess that is where my code changed the most. And that was the
*    challenging part. :)
*
************************************************************************/

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <ctype.h>

struct Timestamp
{
   unsigned long epoch;
};

struct AccessRecord
{
   std::string username;
   std::string filename;
   Timestamp timestamp;
};

struct Log
{
   std::string path;
   Timestamp begin;
   Timestamp end;
};

/**********************************************************************
 * Function: promptFile
 * Purpose: Asks for input file
 ***********************************************************************/
void promptFile(Log &log)
{
   std::cout << "Enter the access record file: ";
   std::getline(std::cin, log.path);

   return;
}

/**********************************************************************
 * Function: promptTimeRange
 * Purpose: Asks for the timestamp range
 ***********************************************************************/
void promptTimeRange(Log &log)
{
   std::cout << "\n";

   std::cout << "Enter the start time: ";
   std::cin >> log.begin.epoch;
   std::cin.ignore(1024, '\n');

   std::cout << "Enter the end time: ";
   std::cin >> log.end.epoch;
   std::cin.ignore(1024, '\n');

   std::cout << "\n";

   // 1442000000 1442332025

   return;
}

/**********************************************************************
 * Function: filterTimestamp
 * Purpose: Determines if the record should be displayed based on a time filter
 ***********************************************************************/
bool filterTimestamp(const unsigned long ts, const Log &l)
{
   // styleChecker crap
   bool result = !(ts >= l.begin.epoch && ts <= l.end.epoch);
   return result;
}

/**********************************************************************
 * Function: parseLine
 * Purpose: Attempts to parse a line and determines if it is corrupt
 ***********************************************************************/
void parseLine(const std::string line)
{
   std::stringstream ss(line);
   // ss.str(line); // load the line into "ss"
   
   std::string a;
   std::string b;
   std::string c;
   ss >> b >> c >> a;

   try
   {
      if (isalpha(a[0]))
      {
         throw line;
      }

      if (atol(a.c_str()) < 1000000000)
      {
         throw line;
      }

      if (atol(a.c_str()) > 10000000000)
      {
         throw line;
      }

      if (a.empty())
      {
         throw line;
      }

      if (ss.fail())
      {
         throw line;
      }
   }
   catch (const std::string message)
   {
      std::cout
         << "Error parsing line: " << message << "\n";
   }
}

/**********************************************************************
 * Function: readFile
 * Purpose: Reads and parses the log file
 ***********************************************************************/
void readFile(std::vector < AccessRecord > &accessRecords, Log &l)
{

   std::ifstream file(l.path);
   // styleChecker crap
   std::string line;

   while (std::getline(file, line))
   {
      // attempts to parse the line
      parseLine(line);
      // creates a new stringstream
      std::stringstream ss;
      ss.str(line);
      // insert stream values into AccessRecord
      accessRecords.push_back(AccessRecord());
      ss >> accessRecords.back().filename;
      ss >> accessRecords.back().username;
      ss >> accessRecords.back().timestamp.epoch;
   }

   return;
}

/**********************************************************************
 * Function: display
 * Purpose: Displays the log parsed results in the console
 ***********************************************************************/
void display(std::vector < AccessRecord > &accessRecords, Log &l)
{
   std::cout << "The following records match your criteria:\n\n";

   //       Timestamp                File                User
   // --------------- ------------------- -------------------
   //      1442000124           house.pdf             jatkins
   //      1442001032           users.txt     kevin_tomlinson
   //      1442210121        accounts.mdb     kevin_tomlinson
   //      1442300125        vacation.jpg            smitty83
   // End of records

   std::cout << "      Timestamp                File                User\n";
   std::cout << "--------------- ------------------- -------------------\n";
   
   for (int i = 0; i < accessRecords.size(); i++)
   {
      if (filterTimestamp(accessRecords[i].timestamp.epoch, l))
      {
         continue;
      }
      std::cout << std::setw(15) << accessRecords[i].timestamp.epoch << " ";
      std::cout << std::setw(19) << accessRecords[i].filename << " ";
      std::cout << std::setw(19) << accessRecords[i].username << "\n";
   }

   std::cout << "End of records\n";

   return;
}

/**********************************************************************
 * Function: main
 * Purpose: Application entrypoint
 ***********************************************************************/
int main()
{
   Log acmeLog;
   promptFile(acmeLog);

   std::vector < AccessRecord > accessRecords;
   readFile(accessRecords, acmeLog);

   promptTimeRange(acmeLog);
   display(accessRecords, acmeLog);

   return 0;
}

/***************************************************************
 * File: product.h
 * Author: Evandro Camargo
 * Purpose: Contains the definition of the Product class
 ***************************************************************/
#ifndef PRODUCT_H
#define PRODUCT_H

#include <string>

/**********************************************************************
 * Class: Product
 * Purpose: Defines the structure of the "Product" class
 ***********************************************************************/
class Product
{
   private:
      // Private Data Members
      std::string name;
      std::string description;
      double weight;
      double price;
      // Private Getters
      double getSalesTax();
      double getShippingCost();
      // Misc
      void clearInputError();

   public:
      // Class Constructors
      Product();
      Product(
         std::string _name,
         std::string _description,
         double _price,
         double _weight
      );
      // Getters
      std::string getName() const;
      std::string getDescription() const;
      double getBasePrice() const;
      double getWeight() const;
      double getTotalPrice();
      // Setters
      void setName(std::string _name);
      void setDescription(std::string _description);
      void setBasePrice(double _price);
      void setWeight(double _weight);
      // Misc
      void prompt();
      void displayAdvertising();
      void displayInventory();
      void displayReceipt();
};

#endif // PRODUCT_H

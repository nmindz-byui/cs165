/***************************************************************
 * File: address.h
 * Author: Evandro Camargo
 * Purpose: Contains the definition of the Address class
 ***************************************************************/

#ifndef ADDRESS_H
#define ADDRESS_H

#include <string>

/**********************************************************************
 * Class: Address
 * Purpose: Defines the structure of the "Address" class
 ***********************************************************************/
class Address
{
   private:
      std::string street;
      std::string city;
      std::string state;
      std::string zip;

   public:
      // Class Constructors
      Address();
      Address(
         std::string _street,
         std::string _city,
         std::string _state,
         std::string _zip
      );
      // Getters
      std::string getStreet() const;
      std::string getCity() const;
      std::string getState() const;
      std::string getZip() const;
      // Setters
      void setStreet(std::string _street);
      void setCity(std::string _city);
      void setState(std::string _state);
      void setZip(std::string _zip);
      // Misc
      void display();

};

#endif // ADDRESS_H
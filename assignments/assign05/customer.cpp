/***************************************************************
 * File: customer.cpp
 * Author: Evandro Camargo
 * Purpose: Contains the method implementations for the Customer class.
 ***************************************************************/

#include <iostream>
#include <string>
#include "customer.h"

/**********************************************************************
 * Function: Customer()
 * Purpose: Default Constructor
 ***********************************************************************/
Customer :: Customer()
{
   name = "unspecified";
   address = Address();
}

/**********************************************************************
 * Function: Customer(std::string, Address)
 * Purpose: Non-Default Constructor with 2 member initalization
 ***********************************************************************/
Customer :: Customer(
   std::string _name,
   Address _address
) {
   setName(_name);
   setAddress(_address);
}

/**********************************************************************
 * Function: getName()
 * Purpose: Name getter function
 ***********************************************************************/
std::string Customer :: getName() const
{
   return name;
}

/**********************************************************************
 * Function: getAddress()
 * Purpose: Address getter function
 ***********************************************************************/
Address Customer :: getAddress() const
{
   return address;
}

/**********************************************************************
 * Function: setName
 * Purpose: Name setter function
 ***********************************************************************/
void Customer :: setName(std::string _name)
{
   name = _name;
}

/**********************************************************************
 * Function: setAddress
 * Purpose: Address setter function
 ***********************************************************************/
void Customer :: setAddress(Address _address)
{
   address = _address;
}

/**********************************************************************
 * Function: display()
 * Purpose: Displays formatted data
 ***********************************************************************/
void Customer :: display()
{
   std::cout
      << name << "\n";

   address.display();
}
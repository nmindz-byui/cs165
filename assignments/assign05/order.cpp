/***************************************************************
 * File: order.cpp
 * Author: Evandro Camargo
 * Purpose: Contains the method implementations for the Order class.
 ***************************************************************/

#include <iostream>
#include <string>
#include "order.h"

/**********************************************************************
 * Function: Order()
 * Purpose: Default Constructor
 ***********************************************************************/
Order :: Order()
{
   product = Product();
   quantity = 0;
   customer = Customer();
}

/**********************************************************************
 * Function: Order(std::string, std::string, std::string, std::string)
 * Purpose: Non-Default Constructor with 4 member initalization
 ***********************************************************************/
Order :: Order(
   Product _product,
   int _quantity,
   Customer _customer
) {
   setProduct(_product);
   setQuantity(_quantity);
   setCustomer(_customer);
}

/**********************************************************************
 * Function: getProduct()
 * Purpose: Product getter function
 ***********************************************************************/
Product Order :: getProduct() const
{
   return product;
}

/**********************************************************************
 * Function: getQuantity()
 * Purpose: Quantity getter function
 ***********************************************************************/
int Order :: getQuantity() const
{
   return quantity;
}

/**********************************************************************
 * Function: getCustomer()
 * Purpose: Customer getter function
 ***********************************************************************/
Customer Order :: getCustomer() const
{
   return customer;
}

/**********************************************************************
 * Function: getShippingZip()
 * Purpose: Shipping ZIP getter function
 ***********************************************************************/
std::string Order :: getShippingZip() const
{
   return customer.getAddress().getZip();
}

/**********************************************************************
 * Function: getTotalPrice()
 * Purpose: Total Price getter function
 ***********************************************************************/
double Order :: getTotalPrice()
{
   return product.getTotalPrice() * quantity;
}

/**********************************************************************
 * Function: setProduct
 * Purpose: Product setter function
 ***********************************************************************/
void Order :: setProduct(Product _product)
{
   product = _product;
}

/**********************************************************************
 * Function: setQuantity
 * Purpose: Quantity setter function
 ***********************************************************************/
void Order :: setQuantity(int _quantity)
{
   quantity = _quantity;
}

/**********************************************************************
 * Function: setCustomer
 * Purpose: Customer setter function
 ***********************************************************************/
void Order :: setCustomer(Customer _customer)
{
   customer = _customer;
}

/**********************************************************************
 * Function: displayShippingLabel()
 * Purpose: Displays formatted data
 ***********************************************************************/
void Order :: displayShippingLabel()
{
   customer.display();
}

/**********************************************************************
 * Function: displayInformation()
 * Purpose: Displays formatted data
 ***********************************************************************/
void Order :: displayInformation()
{
   std::cout 
      << customer.getName() << "\n"
      << product.getName() << "\n"
      << "Total Price: $"
      << product.getTotalPrice() * quantity << "\n";
}
/***************************************************************
 * File: order.h
 * Author: Evandro Camargo
 * Purpose: Contains the definition of the Order class
 ***************************************************************/

#ifndef ORDER_H
#define ORDER_H

#include "product.h"
#include "customer.h"

/**********************************************************************
 * Class: Order
 * Purpose: Defines the structure of the "Order" class
 ***********************************************************************/
class Order
{
   private:
      // Private Data Members
      Product product;
      int quantity;
      Customer customer;

   public:
      // Class Constructors
      Order();
      Order(
         Product _product,
         int _quantity,
         Customer _customer
      );
      // Getters
      Product getProduct() const;
      int getQuantity() const;
      Customer getCustomer() const;
      std::string getShippingZip() const; // Returns the Zip from the customer's address
      double getTotalPrice(); // Returns the total price of the product (including tax and shipping) multiplied by the quantity.
      // Setters
      void setProduct(Product _product);
      void setQuantity(int _quantity);
      void setCustomer(Customer _customer);
      // Misc
      void displayShippingLabel(); // Calls the customer's display method, resulting in this format:
      void displayInformation(); // Displays the customer's name, the product's name, and the total price of the order in this format (substituting for the correct values):
};

   


#endif // ORDER_H
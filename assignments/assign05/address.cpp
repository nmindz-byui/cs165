/***************************************************************
 * File: address.cpp
 * Author: Evandro Camargo
 * Purpose: Contains the method implementations for the Address class.
 ***************************************************************/

#include <iostream>
#include <string>
#include "address.h"

/**********************************************************************
 * Function: Address()
 * Purpose: Default Constructor
 ***********************************************************************/
Address :: Address()
{
   street = "unknown";
   city = "";
   state = "";
   zip = "00000";
}

/**********************************************************************
 * Function: Address(std::string, std::string, std::string, std::string)
 * Purpose: Non-Default Constructor with 4 member initalization
 ***********************************************************************/
Address :: Address(
   std::string _street,
   std::string _city,
   std::string _state,
   std::string _zip
) {
   setStreet(_street);
   setCity(_city);
   setState(_state);
   setZip(_zip);
}

/**********************************************************************
 * Function: getStreet()
 * Purpose: Street getter function
 ***********************************************************************/
std::string Address :: getStreet() const
{
   return street;
}

/**********************************************************************
 * Function: getCity()
 * Purpose: City getter function
 ***********************************************************************/
std::string Address :: getCity() const
{
   return city;
}

/**********************************************************************
 * Function: getState()
 * Purpose: State getter function
 ***********************************************************************/
std::string Address :: getState() const
{
   return state;
}

/**********************************************************************
 * Function: getZip()
 * Purpose: Zip getter function
 ***********************************************************************/
std::string Address :: getZip() const
{
   return zip;
}

/**********************************************************************
 * Function: setStreet
 * Purpose: Street setter function
 ***********************************************************************/
void Address :: setStreet(std::string _street)
{
   street = _street;
}

/**********************************************************************
 * Function: setCity
 * Purpose: City setter function
 ***********************************************************************/
void Address :: setCity(std::string _city)
{
   city = _city;
}

/**********************************************************************
 * Function: setState
 * Purpose: State setter function
 ***********************************************************************/
void Address :: setState(std::string _state)
{
   state = _state;
}

/**********************************************************************
 * Function: setZip
 * Purpose: Zip setter function
 ***********************************************************************/
void Address :: setZip(std::string _zip)
{
   zip = _zip;
}

/**********************************************************************
 * Function: display()
 * Purpose: Displays formatted data
 ***********************************************************************/
void Address :: display()
{
   std::cout
      << street << "\n"
      << city << ", "
      << state << " "
      << zip << "\n";
}
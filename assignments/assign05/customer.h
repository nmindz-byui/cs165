/***************************************************************
 * File: customer.h
 * Author: Evandro Camargo
 * Purpose: Contains the definition of the Customer class
 ***************************************************************/

#ifndef CUSTOMER_H
#define CUSTOMER_H

#include "address.h"

#include <string>

/**********************************************************************
 * Class: Customer
 * Purpose: Defines the structure of the "Customer" class
 ***********************************************************************/
class Customer
{
   private:
      std::string name;
      Address address;

   public:
      // Class Constructors
      Customer();
      Customer(
         std::string _name,
         Address _address
      );
      // Getters
      std::string getName() const;
      Address getAddress() const;
      // Setters
      void setName(std::string _name);
      void setAddress(Address _address);
      // Misc
      void display();

};

#endif // CUSTOMER_H
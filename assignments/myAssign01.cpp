/***********************************************************************
* Program:
*    Assignment 01, Genetic Genealogy  
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary: 
*    This is my version of a DNA comparing tool.
*
*    Estimated:  1.0 hrs   
*    Actual:     1.5 hrs
*      
*    The most challenging part to me was to actually understand
*    from the assignment description how the gene similarity was
*    calculated, until I realized it was just a 10% for each
*    matching gene in order.
*
************************************************************************/

#include <iostream>
#include <string>

/**********************************************************************
 * Function: beginPrompt
 * Purpose: Prompts the user for the DNA sequence and number of relatives
 ***********************************************************************/
void beginPrompt(char* dnaNucleotideSequence, int* relativesCount)
{
   char buffer[10];
   std::cout << "Enter your DNA sequence: ";
   // i.e.: "ATTCGACTGA"
   std::cin.getline(dnaNucleotideSequence, 11);

   std::cout << "Enter the number of potential relatives: ";
   // i.e.: 3
   std::cin >> *relativesCount;
   std::cin.ignore(1024, '\n');
   std::cout << "\n";

   return;
}

/**********************************************************************
 * Function: promptRelativesName
 * Purpose: Prompts for each relative name
 ***********************************************************************/
void promptRelativesName(char** relatives, int count)
{
   for(int i = 0; i < count; i++)
   {
      std::cout << "Please enter the name of relative #" << i + 1 << ": ";
      std::cin.getline(relatives[i], 256);
   }
   std::cout << "\n";

   return;
}

/**********************************************************************
 * Function: promptRelativesDNA
 * Purpose: Prompts for each relative DNA genes
 ***********************************************************************/
void promptRelativesDNA(char** relatives, char** relativesDNA, int count)
{
   for(int i = 0; i < count; i++)
   {
      std::cout << "Please enter the DNA sequence for " << relatives[i] << ": ";
      // i.e.: "ATTCGACTGA"
      std::cin.getline(relativesDNA[i], 11);
   }
   std::cout << "\n";

   return;
}

/**********************************************************************
 * Function: displayResults
 * Purpose: Digests DNA information and displays result for each relative
 ***********************************************************************/
void displayResults(
   char* dnaNucleotideSequence,
   char** relatives,
   char** relativesDNA,
   int count
)
{
   int* match = new int[count];

   // check for DNA gene matches
   for(int i = 0; i < count; i++)
   {
      for(int j = 0; j < 10; j++)
      {
         relativesDNA[i][j] == dnaNucleotideSequence[j] ? match[i]++ : match[i];
      }
   }
   
   // prints DNA % match for each relative
   for(int i = 0; i < count; i++)
   {
      std::cout << "Percent match for "
         << relatives[i]
         << ": "
         << match[i]*10
         << "%"
         << "\n";
   }

   delete[] match;

   return;
}

/**********************************************************************
 * Function: main
 * Purpose: Application entrypoint
 ***********************************************************************/
int main()
{
   char dnaNucleotideSequence[10];
   int* relativesCount = new int;

   // ask for initial DNA and relative count
   beginPrompt(dnaNucleotideSequence, relativesCount);

   // declares relatives-related arrays
   char** relatives = new char*[*relativesCount];
   char** relativesDNA = new char*[*relativesCount];

   for(int i = 0; i < *relativesCount; i++)
   {
      // initialize relatives array
      relatives[i] = new char[256];
      memset(relatives[i], 0, 256);

      // initialize relatives's DNA array
      relativesDNA[i] = new char[10];
      memset(relativesDNA[i], 0, 10);
   }

   promptRelativesName(relatives, *relativesCount);
   promptRelativesDNA(relatives, relativesDNA, *relativesCount);
   displayResults(dnaNucleotideSequence, relatives, relativesDNA, *relativesCount);

   delete[] relatives;
   delete[] relativesDNA;
   delete[] relativesCount;

   return 0;
}

/***********************************************************************
* Program:
*    Checkpoint 02b, Complex Numbers
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary:
*    Summaries are not necessary for checkpoint assignments.
* ***********************************************************************/

#include <iostream>
// using namespace std;

struct Complex {
   double real;
   double imaginary;
};

/**********************************************************************
 * Function: prompt
 * Purpose: Prompts the user for the input values to a Complex number
 ***********************************************************************/
void prompt(Complex &x)
{
   std::cout << "Real: ";
   std::cin >> x.real ;
   std::cin.ignore(1024, '\n');

   std::cout << "Imaginary: ";
   std::cin >> x.imaginary ;
   std::cin.ignore(1024, '\n');
}

/**********************************************************************
 * Function: display
 * Purpose: Displays the formatted result of the program
 ***********************************************************************/
void display(const Complex &sum)
{
   std::cout << sum.real
      << " + "
      << sum.imaginary
      << "i";
}

/**********************************************************************
 * Function: addComplex
 * Purpose: Adds two complex numbers together and returns the sum.
 ***********************************************************************/
Complex addComplex(const Complex &x, const Complex &y)
{
   Complex z;

   z.real = x.real + y.real;
   z.imaginary = x.imaginary + y.imaginary;

   return z;
}

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   Complex c1, c2;

   prompt(c1);
   prompt(c2);

   Complex sum;
   sum = addComplex(c1, c2);

   std::cout << "\nThe sum is: ";
   display(sum);
   std::cout << "\n";

   return 0;
}
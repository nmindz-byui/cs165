/***********************************************************************
* Program:
*    Checkpoint 01b, C++ Basics
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary:
*    Summaries are not necessary for checkpoint assignments.
* ***********************************************************************/

#include <iostream>

/**********************************************************************
 * Function: getSize
 * Purpose: Prompts for the size of the list, or the number of
 * integers to be read in.
 ***********************************************************************/
int getSize()
{
    int x = 0;
    std::cin >> x ;
    return x;
}
/**********************************************************************
 * Function: getList
 * Purpose: Accepts an array of integers and a count. Prompts the user
 * for the list of numbers and store them in the passed array.
 ***********************************************************************/
void getList(int numbers[], int count)
{
   for(int i = 0; i < count; i++)
   {
      std::cout << "Enter number for index " << i << ": ";
      std::cin >> numbers[i];
   }
   std::cout << "\n";

   return;
}
/**********************************************************************
 * Function: displayMultiples
 * Purpose: Accepts the array and its size. It loops through each
 * element in the array and display it if it is divisible by 3.
 ***********************************************************************/
void displayMultiples(int numbers[], int count)
{
   std::cout << "The following are divisible by 3:\n";

    for(int i = 0; i < count; i++)
   {
      if (numbers[i] % 3 == 0)
      {
         std::cout << numbers[i] << "\n";
      }
   }
   return;
}
/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   std::cout << "Enter the size of the list: ";
   int count = getSize();

   int* numbers = new int[count];
   getList(numbers, count);

   displayMultiples(numbers, count);

   delete[] numbers;

   return 0;
}
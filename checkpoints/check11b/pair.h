/*****************************************************
 * File: pair.h
 *
 * Purpose: Defines a Pair template class that can hold
 *   two items of arbitrary data types.
 ******************************************************/

#ifndef PAIR_H
#define PAIR_H

template <class T, class P>
class Pair
{
   private:
      T first;
      P second;
   public:
      // Getters
      T getFirst() const
      {
         return this->first;
      };
      P getSecond() const
      {
         return this->second;
      };
      // Setters
      void setFirst(T _first)
      {
         this->first = _first;
      };
      void setSecond(P _second)
      {
         this->second = _second;
      };
      // Misc
      void display() const
      {
         std::cout
            << this->getFirst()
            << " - "
            << this->getSecond();
      }
};

#endif // PAIR_H

#include "numberList.h"

#include <iostream>
using namespace std;

/******************************************************
 * Function: NumberList
 * Description: Non Default Constructor, +1 Overload
 ******************************************************/
NumberList :: NumberList(int _integer)
{
   this->size = _integer;
   this->array = new int[this->size];

   for (int i; i < _integer; i++)
   {
      this->array[i] = 0;
   }
}

/******************************************************
 * Function: ~NumberList
 * Description: Default Destructor
 ******************************************************/
NumberList :: ~NumberList()
{
   delete[] this->array;
   this->array = NULL;

   std::cout << "Freeing memory\n";
}

/******************************************************
 * Function: getNumber
 * Description: Returns the number at the given index.
 ******************************************************/
int NumberList::getNumber(int index) const
{
   int number = -1;

   if (index >= 0 && index < size)
   {
      number = array[index];
   }

   return number;
}

/******************************************************
 * Function: setNumber
 * Description: Sets the value to the array at the given index.
 ******************************************************/
void NumberList::setNumber(int index, int value)
{
   if (index >= 0 && index < size)
   {
      array[index] = value;
   }
}

/******************************************************
 * Function: displayList
 * Description: displays the list
 ******************************************************/
void NumberList::displayList() const
{
   for (int i = 0; i < size; i++)
   {
      cout << array[i] << endl;
   }

   cout << endl;
}


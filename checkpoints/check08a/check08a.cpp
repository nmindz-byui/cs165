/***********************************************************************
* Program:
*    Checkpoint 04a, Classes
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary:
*    Summaries are not necessary for checkpoint assignments.
* ***********************************************************************/

#include <iostream>
#include <string>
#include "book.h"
#include "textbook.h"
#include "picturebook.h"

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   Book aBook;
   TextBook aTextBook;
   PictureBook aPictureBook;

   aBook.promptBookInfo();
   std::cout
      << "\n";
   aBook.displayBookInfo();
   std::cout
      << "\n";

   aTextBook.promptBookInfo();
   aTextBook.promptSubject();
   std::cout
      << "\n";
   aTextBook.displayBookInfo();
   aTextBook.displaySubject();
   std::cout
      << "\n";

   aPictureBook.promptBookInfo();
   aPictureBook.promptIllustrator();
   std::cout
      << "\n";
   aPictureBook.displayBookInfo();
   aPictureBook.displayIllustrator();
}
/********************************************************************
 * File: textbook.h
 * Purpose: Holds the definition of the TextBook class.
 ********************************************************************/

#ifndef TEXTBOOK_H
#define TEXTBOOK_H

#include <iostream>
#include <string>
#include "book.h"

/**********************************************************************
 * Class: TextBook
 * Purpose: Defines the structure of the "TextBook" class
 ***********************************************************************/
class TextBook : public Book
{
   private:
      std::string subject;

   public:
      void promptSubject();
      void displaySubject();
};

#endif // TEXTBOOK_H
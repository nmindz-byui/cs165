/********************************************************************
 * File: picturebook.h
 * Purpose: Holds the definition of the PictureBook class.
 ********************************************************************/

#ifndef PICTUREBOOK_H
#define PICTUREBOOK_H

#include <iostream>
#include <string>
#include "book.h"

/**********************************************************************
 * Class: PictureBook
 * Purpose: Defines the structure of the "PictureBook" class
 ***********************************************************************/
class PictureBook : public Book
{
   private:
      std::string illustrator;

   public:
      void promptIllustrator();
      void displayIllustrator();
};

#endif // PICTUREBOOK_H
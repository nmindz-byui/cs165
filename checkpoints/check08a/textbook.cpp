/********************************************************************
 * File: textbook.cpp
 * Purpose: Holds the implementation of the TextBook class methods.
 ********************************************************************/

#include "./textbook.h"

/**********************************************************************
 * Function: promptSubject
 * Purpose: Prompts for the Book Subject
 ***********************************************************************/
void TextBook :: promptSubject()
{
   std::cout << "Subject: ";
   std::getline(std::cin, this->subject);
}

/**********************************************************************
 * Function: displaySubject
 * Purpose: Displays the Book Subject
 ***********************************************************************/
void TextBook :: displaySubject()
{
   std::cout
      << "Subject: "
      << this->subject
      << "\n";
}
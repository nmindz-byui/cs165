/********************************************************************
 * File: picturebook.cpp
 * Purpose: Holds the implementation of the PictureBook class methods.
 ********************************************************************/

#include "./picturebook.h"

/**********************************************************************
 * Function: promptIllustrator
 * Purpose: Prompts for the Book Illustrator
 ***********************************************************************/
void PictureBook :: promptIllustrator()
{
   std::cout << "Illustrator: ";
   std::getline(std::cin, this->illustrator);
}

/**********************************************************************
 * Function: displayIllustrator
 * Purpose: Displays the Book Illustrator
 ***********************************************************************/
void PictureBook :: displayIllustrator()
{
   std::cout
      << "Illustrated by "
      << this->illustrator
      << "\n";
}
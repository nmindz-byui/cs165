/********************************************************************
 * File: book.cpp
 * Purpose: Holds the implementation of the Book class methods.
 ********************************************************************/

#include "./book.h"

/**********************************************************************
 * Function: promptBookInfo
 * Purpose: Prompts for the Book Information
 ***********************************************************************/
void Book :: promptBookInfo()
{
   std::cout << "Title: ";
   std::getline(std::cin, this->title);
   
   std::cout << "Author: ";
   std::getline(std::cin, this->author);
   
   std::cout << "Publication Year: ";
   std::cin >> this->publicationYear;
   std::cin.ignore();
}

/**********************************************************************
 * Function: displayBookInfo
 * Purpose: Displays the Book Information
 ***********************************************************************/
void Book :: displayBookInfo()
{
   std::cout
      << this->title
      << " ("
      << this->publicationYear
      << ")"
      << " by "
      << this->author
      << "\n";
}
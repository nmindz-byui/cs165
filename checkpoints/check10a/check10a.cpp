/***********************************************************************
* Program:
*    Checkpoint 10a, Vectors
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary: 
*    Summaries are not necessary for checkpoint assignments.
************************************************************************/

#include <iostream>
#include <string>
#include <vector>

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   std::vector<int> intVect;
   std::vector<std::string> strVect;
   int intInput = 0;
   std::string strInput = "";

   // Loop for int vector inputs
   do
   {
      std::cout
         << "Enter int: ";
      std::cin >> intInput;
      intVect.push_back(intInput);
   }
   while (intInput != 0);

   // Print list of ints, one element per line
   std::cout << "Your list is:\n";

   for (std::vector<int> :: iterator it = intVect.begin(); it != intVect.end() - 1; it++)
   {
      std::cout << *it << "\n";
   }

   // Make space for next iteration
   // and fix the input buffer
   std::cout << "\n";
   std::cin.ignore();

   // Loop for string vector inputs
   do
   {
      std::cout
         << "Enter string: ";
      std::getline(std::cin, strInput);
      strVect.push_back(strInput);
   }
   while (strInput != "quit");

   // Print list of strings, one element per line
   std::cout << "Your list is:\n";

   for (std::vector<std::string> :: iterator it = strVect.begin(); it != strVect.end() - 1; it++)
   {
      std::cout << *it << "\n";
   }

   return 0;
}



/***********************************************************************
* Program:
*    Checkpoint 09a, Virtual Functions
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary: 
*    Summaries are not necessary for checkpoint assignments.
* ***********************************************************************/

#include <iostream>
#include <string>

// For this assignment, for simplicity, you may put all of your classes
// in this file.

/**********************************************************************
 * Class: Car
 * Purpose: Defines the structure of the "Car" class
 ***********************************************************************/

#ifndef CAR_H
#define CAR_H

class Car
{
   protected:
      std::string name;
   private:

   public:
      Car() { this->setName("Unknown model"); }
      std::string getName() const { return this->name; };
      virtual std::string getDoorSpecs() { return "Unknown doors"; };
      void setName(std::string _name) { this->name = _name; };
};

#endif // CAR_H

/**********************************************************************
 * Class: Civic
 * Purpose: Defines the structure of the "Civic" class
 ***********************************************************************/

#ifndef CIVIC_H
#define CIVIC_H

class Civic : public Car
{
   protected:
      std::string name;
   private:

   public:
      Civic() { this->setName("Civic"); }
      virtual std::string getDoorSpecs() { return "4 doors"; };
};

#endif // CIVIC_H

/**********************************************************************
 * Class: Odyssey
 * Purpose: Defines the structure of the "Odyssey" class
 ***********************************************************************/

#ifndef ODYSSEY_H
#define ODYSSEY_H

class Odyssey : public Car
{
   protected:
      std::string name;
   private:

   public:
      Odyssey() { this->setName("Odyssey"); }
      virtual std::string getDoorSpecs() { return "2 front doors, 2 sliding doors, 1 tail gate"; };
};

#endif // ODYSSEY_H

/**********************************************************************
 * Class: Ferrari
 * Purpose: Defines the structure of the "Ferrari" class
 ***********************************************************************/

#ifndef FERRARI_H
#define FERRARI_H

class Ferrari : public Car
{
   protected:
      std::string name;
   private:

   public:
      Ferrari() { this->setName("Ferrari"); }
      virtual std::string getDoorSpecs() { return "2 butterfly doors"; };
};

#endif // FERRARI_H

/**********************************************************************
 * Function: attachDoors
 * Purpose: This function can accept any type of Car object. It will
 *  call the appropriate functions to display the name and the doors info.
 ***********************************************************************/

void attachDoors(Car &pCar)
{
   std::cout
      << "Attaching doors to "
      << pCar.getName()
      << " - "
      << pCar.getDoorSpecs()
      << "\n";
}

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   // You should not change main

   Civic civic;
   Odyssey odyssey;
   Ferrari ferrari;

   attachDoors(civic);
   attachDoors(odyssey);
   attachDoors(ferrari);

   return 0;
}
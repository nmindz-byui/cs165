/***********************************************************************
* Program:
*    Checkpoint 03a, Exceptions
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary:
*    Summaries are not necessary for checkpoint assignments.
* ***********************************************************************/

#include <iostream>

#define EID_NUMBER_OK         0
#define EID_NUMBER_NEGATIVE   1
#define EID_NUMBER_TOO_BIG    2
#define EID_NUMBER_ODD        3

/**********************************************************************
 * Function: testNumber
 * Purpose: Checks number and returns the appropriate flag
 ***********************************************************************/
int testNumber(int number)
{
   if (number < 0)
   {
      return EID_NUMBER_NEGATIVE;
   }
   else if (number > 100)
   {
      return EID_NUMBER_TOO_BIG;
   }
   else if (number % 2 != 0)
   {
      return EID_NUMBER_ODD;
   }
   else
   {
      return EID_NUMBER_OK;
   }
}

/**********************************************************************
 * Function: display
 * Purpose: Display the result at the console after user input
 ***********************************************************************/
int display(int number)
{
   switch (testNumber(number))
   {
      case EID_NUMBER_NEGATIVE:
         throw "The number cannot be negative.";
         break;
      case EID_NUMBER_TOO_BIG:
         throw "The number cannot be greater than 100.";
         break;
      case EID_NUMBER_ODD:
         throw "The number cannot be odd.";
         break;
      case EID_NUMBER_OK:
         std::cout << "The number is " << number << ".\n";
         break;
   
      default:
         break;
   }
}

/**********************************************************************
 * Function: prompt
 * Purpose: Prompts for user primary input
 ***********************************************************************/
int prompt()
{
   int number = 0;
   std::cout << "Enter a number: ";
   std::cin >> number;

   return number;
}

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   int inputNumber = prompt();

   try
   {
      display(inputNumber);
   }
   catch (const char * message)
   {
      std::cout << "Error: " << message << "\n";
   }

   return 0;
}
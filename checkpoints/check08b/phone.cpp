/********************************************************************
 * File: phone.cpp
 * Purpose: Holds the implementation of the Phone class methods.
 ********************************************************************/

#include "phone.h"

/**********************************************************************
 * Function: promptNumber
 * Purpose: Prompts for the Phone Number
 ***********************************************************************/
void Phone :: promptNumber()
{
   std::cout << "Area Code: ";
   std::cin >> this->areaCode;
   
   std::cout << "Prefix: ";
   std::cin >> this->prefix;
   
   std::cout << "Suffix: ";
   std::cin >> this->suffix;
   std::cin.ignore();
}

/**********************************************************************
 * Function: display
 * Purpose: Displays the Phone properties
 ***********************************************************************/
void Phone :: display() const
{
   std::cout
      << "Phone info:\n"
      << "(" << this->areaCode
      << ")" << this->prefix
      << "-" << this->suffix
      << "\n";
}
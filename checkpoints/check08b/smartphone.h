/********************************************************************
 * File: phone.h
 * Purpose: Holds the definition of the SmartPhone class.
 ********************************************************************/

#ifndef SMARTPHONE_H
#define SMARTPHONE_H

#include <string>
#include "phone.h"

/**********************************************************************
 * Class: SmartPhone
 * Purpose: Defines the structure of the "SmartPhone" class
 ***********************************************************************/
class SmartPhone : public Phone
{
   private:
      std::string email;

   public:
      void prompt();
      void display() const;
};

#endif // SMARTPHONE_H
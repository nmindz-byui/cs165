/********************************************************************
 * File: smartphone.cpp
 * Purpose: Holds the implementation of the SmartPhone class methods.
 ********************************************************************/

#include "smartphone.h"

/**********************************************************************
 * Function: prompt
 * Purpose: Prompts for the SmartPhone Details
 ***********************************************************************/
void SmartPhone :: prompt()
{
   Phone::promptNumber();
   
   std::cout << "Email: ";
   std::getline(std::cin, this->email);
}

/**********************************************************************
 * Function: display
 * Purpose: Displays the SmartPhone properties
 ***********************************************************************/
void SmartPhone :: display() const
{
   Phone::display();

   std::cout
      << this->email;
}
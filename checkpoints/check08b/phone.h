/********************************************************************
 * File: phone.h
 * Purpose: Holds the definition of the Phone class.
 ********************************************************************/

#ifndef PHONE_H
#define PHONE_H

#include <iostream>

/**********************************************************************
 * Class: Phone
 * Purpose: Defines the structure of the "Phone" class
 ***********************************************************************/
class Phone
{
   private:
      int areaCode;
      int prefix;
      int suffix;

   public:
      void promptNumber();
      void display() const;
};

#endif // PHONE_H
/***********************************************************************
* Program:
*    Checkpoint 04a, Classes
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary:
*    Summaries are not necessary for checkpoint assignments.
* ***********************************************************************/

#include <iostream>
#include <string>
#include "phone.h"
#include "smartphone.h"

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   Phone aPhone;
   SmartPhone aSmartPhone;

   std::cout << "Phone:\n";
   aPhone.promptNumber();
   std::cout
      << "\n";
   aPhone.display();
   std::cout
      << "\n";

   std::cout << "Smart phone:\n";
   aSmartPhone.prompt();
   std::cout
      << "\n";
   aSmartPhone.display();
   std::cout
      << "\n";
}
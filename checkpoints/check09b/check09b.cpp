/***********************************************************************
* Program:
*    Checkpoint 09b, Pure Virtual Functions
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary: 
*    Summaries are not necessary for checkpoint assignments.
* ***********************************************************************/

#include <iostream>
#include <iomanip>
#include <string>

// For this assignment, for simplicity, you may put all of your classes
// in this file.

class Shape
{
   private:
      std::string name;

   public:
      virtual ~Shape() { }
      std::string getName() const { return name; }
      void setName(std::string _name) { this->name = _name; }
      virtual float getArea() const = 0;
};

class Circle : public Shape
{
   private:
      float radius;

   public:
      Circle() { setName("Circle"); }
      Circle(float _radius) {
         setName("Circle");
         this->setRadius(_radius);
      }
      ~Circle() { std::cout << "Cleaning up " << getName() << "\n"; }
      float getRadius() const { return radius; }
      void setRadius(const float _radius) { this->radius = _radius; }
      float getArea() const { return 3.14 * this->radius * this->radius; }
};

class Rectangle : public Shape
{
private:
   float length;
   float width;

public:
   Rectangle() { setName("Rectangle"); }
   Rectangle(float _length, float _width) {
      setName("Rectangle");
      this->setLength(_length);
      this->setWidth(_width);
   }
   ~Rectangle() { std::cout << "Cleaning up " << getName() << "\n"; }
   float getLength() const { return length; }
   float getWidth() const { return width; }
   void setLength(const float _length) { this->length = _length; }
   void setWidth(const float _width) {this->width = _width; }
   float getArea() const { return this->length * this->width; }
};

const int MAX_SIZE = 10;

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   // We will put all of our code in main for this one, just to keep
   // the focus on the virtual functions and not on passing items
   // between functions.
   
   // TODO: 1. Declare your array here
   // For this assignment you can use the size: MAX_SIZE
   Shape* myShapes[MAX_SIZE];

   char letter;
   int count = 0;

   do
   {
      std::cout << "Please enter 'c' for circle, 'r' for rectangle or 'q' to quit: ";
      std::cin >> letter;

      if (letter == 'c')
      {
         float radius;
         std::cout << "Enter the radius: ";
         std::cin >> radius;

         // TODO: 2. Create your circle object here, set the radius value
         // and add it to the array at index "count".
         Circle* tCircle = new Circle(radius);
         myShapes[count] = tCircle;
         
         count++; // we have seen another shape
      }
      else if (letter == 'r')
      {
         float length;
         float width;

         std::cout << "Enter the length: ";
         std::cin >> length;

         std::cout << "Enter the width: ";
         std::cin >> width;
      
         // TODO: 3. Create your rectangle object here, set the length and
         // width, and add it to the array at index "count".
         Rectangle* tRect = new Rectangle(length, width);
         myShapes[count] = tRect;

         count++; // we have seen another shape
      }

   } while (letter != 'q');

   // Set the precision for our decimals
   std::cout.setf(std::ios::fixed);
   std::cout.setf(std::ios::showpoint);
   std::cout.precision(2);

   // Now we will display each shape
   for (int i = 0; i < count; i++)
   {
      // TODO: 4. Add a cout statment here to display the name and the area
      // of each shape in the list in the format "Circle - 10.32"
      std::cout
         << myShapes[i]->getName()
         << " - "
         << myShapes[i]->getArea()
         << "\n";
   }

   // TODO: 5. Loop through and free the memory of each object.
   for(int i = 0; i < count; i++)
   {
      delete myShapes[i];
      myShapes[i] = NULL;
   }

   return 0;
}



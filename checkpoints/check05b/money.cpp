/***********************
 * File: money.cpp
 ***********************/

#include <iostream>
#include <iomanip>

#include "money.h"

/**********************************************************************
 * Function: Default Constructor
 * Purpose: --------------------
 ***********************************************************************/
Money :: Money()
{
    Money :: setDollars(0);
    Money :: setCents(0);
}

/**********************************************************************
 * Function: Non-Default Constructor #1
 * Purpose: ---------------------------
 ***********************************************************************/
Money :: Money(double _dollars)
{
    Money :: setDollars(_dollars);
    Money :: setCents(0);
}

/**********************************************************************
 * Function: Non-Default Constructor #2
 * Purpose: ---------------------------
 ***********************************************************************/
Money :: Money(double _dollars, double _cents)
{
    Money :: setDollars(_dollars);
    Money :: setCents(_cents);
}

/**********************************************************************
 * Function: getDollars
 * Purpose: Gets the numbers to the left of the period
 ***********************************************************************/
inline double Money :: getDollars() const
{
   return dollars;
}

/**********************************************************************
 * Function: getCents
 * Purpose: Gets the numbers to the right of the period
 ***********************************************************************/
inline double Money :: getCents() const
{
   return cents;
}

/**********************************************************************
 * Function: setDollars
 * Purpose: Sets the numbers to the left of the period
 ***********************************************************************/
inline bool Money :: setDollars(double _dollars)
{
   _dollars < 0 ? dollars = _dollars * -1 : dollars = _dollars;
   return (dollars == _dollars || dollars == (_dollars * -1));
}

/**********************************************************************
 * Function: setCents
 * Purpose: Sets the numbers to the right of the period
 ***********************************************************************/
inline bool Money :: setCents(double _cents)
{
   _cents < 0 ? cents = _cents * -1 : cents = _cents;
   return (cents == _cents || cents == (_cents * -1));
}

/*****************************************************************
 * Function: prompt
 * Purpose: Asks the user for values for dollars and cents
 *   and stores them.
 ****************************************************************/
void Money :: prompt()
{
   int dollars;
   int cents;

   std::cout << "Dollars: ";
   std::cin >> dollars;

   std::cout << "Cents: ";
   std::cin >> cents;

   setDollars(dollars);
   setCents(cents);
}

/*****************************************************************
 * Function: display
 * Purpose: Displays the value of the money object.
 ****************************************************************/
void Money :: display() const 
{
   std::cout << "$" << getDollars() << ".";
   std::cout << std::setfill('0') << std::setw(2) << getCents();
}

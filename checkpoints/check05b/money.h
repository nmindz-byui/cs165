/******************
 * File: money.h
 ******************/
#ifndef MONEY_H
#define MONEY_H

class Money
{
   private:
      int dollars;
      int cents;

   public:
      Money();
      Money(double _dollars);
      Money(double _dollars, double _cents);
      void prompt();
      void display() const;
      double getDollars() const;
      double getCents() const;
      bool setDollars(double _dollars);
      bool setCents(double _cents);
};

#endif // MONEY_H

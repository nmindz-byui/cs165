/******************
 * File: money.h
 ******************/
#ifndef MONEY_H
#define MONEY_H

#include <iostream>

/******************************************************
 * Class: Money
 * Description: Holds a value of dollars and cents.
 ******************************************************/
class Money
{
private:
   int dollars;
   int cents;

public:
   /************************
    * Constructors
    ************************/
   Money()
   {
      setDollars(0);
      setCents(0);
   }

   Money(int dollars, int cents)
   {
      setDollars(dollars);
      setCents(cents);
   }

   /************************
    * Getters and Setters
    ************************/
   int getDollars() const { return dollars; }
   int getCents() const { return cents; }
   
   // These could be done in a smarter way to add cents to dollars if more than 100 etc.
   // but we're trying to keep it simple for this assignment...
   void setDollars(int dollars) { this->dollars = dollars; }
   void setCents(int cents) { this->cents = cents; }

   /************************
    * Other public methods
    ************************/
   void prompt();
   void display() const;
};

// Non-member operator prototypes go here

// == (Equivalence) - Check if the dollars and cents amounts are equal.
bool operator==(const Money & lhs, const Money & rhs);

// != (Not Equal) - the opposite of the == operator.
bool operator!=(const Money & lhs, const Money & rhs);

// << (Stream insertion aka the "output" operator) - This should behave like the display function and allow you to output a Money object to the console, with a statement such as "cout << myMoney;" that would produce output in the following format: $3.45 or: $3.05 or: $0.61
std::ostream & operator<<(ostream & out, const Money & money);

#endif // MONEY_H
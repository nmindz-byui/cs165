/***********************
 * File: money.cpp
 ***********************/

#include <iostream>
#include <iomanip>
using namespace std;

#include "money.h"

/*****************************************************************
 * Function: prompt
 * Purpose: Asks the user for values for dollars and cents
 *   and stores them.
 ****************************************************************/
void Money :: prompt()
{
   int dollars;
   int cents;

   cout << "Dollars: ";
   cin >> dollars;

   cout << "Cents: ";
   cin >> cents;

   setDollars(dollars);
   setCents(cents);
}

/*****************************************************************
 * Function: display
 * Purpose: Displays the value of the money object.
 ****************************************************************/
void Money :: display() const 
{
   cout << "$" << dollars << ".";
   cout << setfill('0') << setw(2) << cents;
}

/*****************************************************************
 * Function: == (Equivalence)
 * Purpose: Check if the dollars and cents amounts are equal.
 ****************************************************************/ 
bool operator == (const Money & lhs, const Money & rhs)
{
   bool dollars = lhs.getDollars() == rhs.getDollars();
   bool cents = lhs.getCents() == rhs.getCents();

   return (dollars && cents);
}

/*****************************************************************
 * Function: != (Not Equal)
 * Purpose: The opposite of the == operator.
 ****************************************************************/ 
bool operator != (const Money & lhs, const Money & rhs)
{
   bool dollars = lhs.getDollars() == rhs.getDollars();
   bool cents = lhs.getCents() == rhs.getCents();

   return !(dollars && cents);
}

/*****************************************************************
 * Function: << - Stream Insertion
 * Purpose: Stream insertion aka the "output" operator.
 ****************************************************************/ 
ostream & operator << (ostream & out, const Money & money)
{
   out << "$" << money.getDollars() << ".";
   out << setfill('0') << setw(2) << money.getCents();
}
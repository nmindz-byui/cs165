/********************************************************************
 * File: date.h
 * Purpose: Holds the definition of the date class.
 ********************************************************************/

#ifndef DATE_H
#define DATE_H

/**********************************************************************
 * Class: Date
 * Purpose: Defines the structure of the "Date" class
 ***********************************************************************/
class Date
{
   private:
      unsigned int year;
      unsigned int month;
      unsigned int day;

   public:
      void set(unsigned int sYear, unsigned int sMonth, unsigned int sDay);
      // This isn't a checkpoint requirement, but I felt like this is something I would do
      // If "extra" code such as this is a problem, please let me know.
      void setYear(unsigned int sYear);
      void setMonth(unsigned int sMonth);
      void setDay(unsigned int sDay);
      // Display functions
      void displayAmerican();
      void displayEuropean();
      void displayISO();
};

#endif // DATE_H
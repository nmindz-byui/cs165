/*********************************************************************
 * File: check04b.cpp
 * Purpose: contains the main method to exercise the Date class.
 *********************************************************************/

#include <iostream>
#include "./date.h"
#include "./date.cpp"

/**********************************************************************
 * Function: prompt
 * Purpose: This function handles the prompting of data
 ***********************************************************************/
void prompt(Date &date)
{
   unsigned int year, month, day = 0;
   
   std::cout << "Month: ";
   std::cin >> month;

   std::cout << "Day: ";
   std::cin >> day;

   std::cout << "Year: ";
   std::cin >> year;

   std::cout << "\n";

   date.set(year, month, day);
}

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   Date mySpecialDate;
   prompt(mySpecialDate);
   
   mySpecialDate.displayAmerican();
   mySpecialDate.displayEuropean();
   mySpecialDate.displayISO();

   return 0;
}
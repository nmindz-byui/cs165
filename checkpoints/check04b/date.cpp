/********************************************************************
 * File: date.cpp
 * Purpose: Holds the implementation of the Date class methods.
 ********************************************************************/

#include <iostream>
#include "./date.h"

/**********************************************************************
 * Function: set
 * Purpose: Sets a complete date
 ***********************************************************************/
void Date :: set(unsigned int sYear, unsigned int sMonth, unsigned int sDay)
{
   year = sYear;
   month = sMonth;
   day = sDay;
}

/**********************************************************************
 * Function: setYear
 * Purpose: Sets the year of the current date object
 ***********************************************************************/
void Date :: setYear(unsigned int sYear)
{
   year = sYear;
}

/**********************************************************************
 * Function: setMonth
 * Purpose: Sets the month of the current date object
 ***********************************************************************/
void Date :: setMonth(unsigned int sMonth)
{
   month = sMonth;
}

/**********************************************************************
 * Function: setDay
 * Purpose: Sets the day of the current date object
 ***********************************************************************/
void Date :: setDay(unsigned int sDay)
{
   day = sDay;
}

/**********************************************************************
 * Function: displayAmerican
 * Purpose: Displays American formatted date
 ***********************************************************************/
void Date :: displayAmerican()
{
   std::cout
      << month << "/"
      << day << "/"
      << year << "\n";
}

/**********************************************************************
 * Function: displayEuropean
 * Purpose: Displays European formatted date
 ***********************************************************************/
void Date :: displayEuropean()
{
   std::cout
      << day << "/"
      << month << "/"
      << year << "\n";
}

/**********************************************************************
 * Function: displayISO
 * Purpose: Displays ISO-8601 standard formatted date
 ***********************************************************************/
void Date :: displayISO()
{
   std::cout
      << year << "-"
      << month << "-"
      << day << "\n";
}

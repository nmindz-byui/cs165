/***********************************************************************
* Program:
*    Checkpoint 02a, Structs
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary:
*    Summaries are not necessary for checkpoint assignments.
* ***********************************************************************/

#include <iostream>


struct User {
  char firstName[256];
  char lastName[256];
  int userId;
} ;

/**********************************************************************
 * Function: promptFirstName
 * Purpose: Prompts for and stores the first name in the referenced User
 ***********************************************************************/
void promptFirstName(User &person)
{
   std::cout << "Please enter your first name: ";
   std::cin.getline(person.firstName, 256);

   return;
}

/**********************************************************************
 * Function: promptLastName
 * Purpose: Prompts for and stores the last name in the referenced User
 ***********************************************************************/
void promptLastName(User &person)
{
   std::cout << "Please enter your last name: ";
   std::cin.getline(person.lastName, 256);

   return;
}

/**********************************************************************
 * Function: promptId
 * Purpose: Prompts for and stores the ID in the referenced User
 ***********************************************************************/
void promptId(User &person)
{
   std::cout << "Please enter your id number: ";
   std::cin >> person.userId;
   std::cin.ignore(1024, '\n');

   return;
}

/**********************************************************************
 * Function: displayInformation
 * Purpose: Displays the formatted end result of the program
 ***********************************************************************/
void displayInformation(User &person)
{
   std::cout << "\nYour information:\n";
   std::cout
      << person.userId
      << " - "
      << person.firstName
      << " "
      << person.lastName
      << "\n";

   return;
}

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   User newPerson;
   promptFirstName(newPerson);
   promptLastName(newPerson);
   promptId(newPerson);

   displayInformation(newPerson);

   return 0;
}
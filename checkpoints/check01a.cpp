/***********************************************************************
* Program:
*    Checkpoint 01a, C++ Basics          (e.g. Checkpoint 01a, review)
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary:
*    Summaries are not necessary for checkpoint assignments.
* ***********************************************************************/

#include <iostream>
// using namespace std;

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   char name[256];
   int age = 0;

   std::cout << "Hello CS 165 World!" << "\n";

   std::cout << "Please enter your first name: ";
   std::cin.getline(name, 256);

   std::cout << "Please enter your age: ";
   std::cin >> age;

   std::cout << "\n"
      << "Hello " << name
      << ", you are " << age
      << " years old." << "\n";

   return 0;
}
/**********************
 * File: check07a.cpp
 **********************/

#include <iostream>
#include <string>

#include "icecream.h"

/*****************************************
 * Function: Main
 * Purpose: Tests the Ice Cream class
 *****************************************/

int main()
{
   IceCream iceCream1;
   iceCream1.prompt();

   IceCream iceCream2;
   iceCream2.prompt();

   std::cout << "\nMenu:\n";
   iceCream1.display();
   iceCream2.display();
   std::cout << "\n";

   float tax;
   std::cout << "Enter alternate sales tax: ";
   std::cin >> tax;

   iceCream1.setSalesTax(tax);
   iceCream2.setSalesTax(tax);

   std::cout << "\nMenu:\n";
   iceCream1.display();
   iceCream2.display();

   return 0;
}

/****************************
 * File: icecream.h
 ****************************/
#ifndef ICECREAM_H
#define ICECREAM_H

#include <string>

class IceCream
{
   private:
      std::string flavor;
      float price;

   public:
      // Public Variables
      float salesTax;
      // Constructors
      IceCream();
      IceCream(std::string _flavor, float _price);
      // Getters
      std::string getFlavor() const;
      float getPrice() const;
      float getSalesTax() const;
      float getTotalPrice() const;
      // Setters
      void setFlavor(std::string _flavor);
      void setPrice(float _price);
      void setSalesTax(float _salexTax);
      // Misc
      void prompt();
      void display() const;
};


#endif // ICECREAM_H
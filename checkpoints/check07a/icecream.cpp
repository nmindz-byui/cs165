/***********************************
 * File: icecream.cpp
 ***********************************/
#include "icecream.h"

#include <iostream>
#include <string>
#include <iomanip>

/***********************************
 * Constructors
 ***********************************/
IceCream :: IceCream()
{
   setFlavor("unknown");
   setPrice(0.0);
   setSalesTax(0.06);
}

IceCream :: IceCream(std::string _flavor, float _price)
{
   setFlavor(_flavor);
   setPrice(_price);
   setSalesTax(0.06);
}

/**********************************************************************
 * Function: IceCream :: getFlavor
 * Purpose: Gets the flavor of the ice cream.
 **********************************************************************/
std::string IceCream :: getFlavor() const
{
   return this->flavor;
}

/**********************************************************************
 * Function: IceCream :: getPrice
 * Purpose: Gets the price of the ice cream.
 **********************************************************************/
float IceCream :: getPrice() const
{
   return this->price;
}

/**********************************************************************
 * Function: IceCream :: getSalesTax
 * Purpose: Gets the salex tax of the ice cream.
 **********************************************************************/
float IceCream :: getSalesTax() const
{
   return this->salesTax;
}

/**********************************************************************
 * Function: IceCream :: getTotalPrice
 * Purpose: Gets the total price of the ice cream, including sales tax.
 **********************************************************************/
float IceCream :: getTotalPrice() const
{
   return this->getPrice() * (this->getSalesTax() + 1);
}

/**********************************************************************
 * Function: IceCream :: setFlavor
 * Purpose: Sets the flavor of the ice cream.
 **********************************************************************/
void IceCream :: setFlavor(std::string _flavor)
{
   this->flavor = _flavor;
}

/**********************************************************************
 * Function: IceCream :: setPrice
 * Purpose: Sets the price of the ice cream.
 **********************************************************************/
void IceCream :: setPrice(float _price)
{
   this->price = _price;
}

/**********************************************************************
 * Function: IceCream :: setSalesTax
 * Purpose: Sets the salex tax of the ice cream.
 **********************************************************************/
void IceCream :: setSalesTax(float _salesTax)
{
   this->salesTax = _salesTax;
}

/**********************************************************************
 * Function: IceCream :: prompt
 * Purpose: Prompts the user for the values of ice cream.
 **********************************************************************/
void IceCream :: prompt()
{
   std::string _flavor;
   float _price = 0.0;

   std::cout << "Flavor: ";
   std::getline(std::cin, _flavor);

   do 
   {
      std::cout << "Price: ";
      std::cin >> _price;
      std::cin.ignore();
   } while (std::cin.fail());

   setFlavor(_flavor);
   setPrice(_price);
}

/**********************************************************************
 * Function: IceCream :: display
 * Purpose: Displays the current ice cream.
 **********************************************************************/
void IceCream :: display() const
{
   std::cout.setf(std::ios::fixed);
   std::cout.setf(std::ios::showpoint);
   std::cout.precision(2);

   // we call the getTotalPrice() method here, so it will
   // automatically updated when sales tax is taken into ac std::cout.
   std::cout << "$" << getTotalPrice() << " - " << flavor << "\n";
}


/***********************************************************************
* Program:
*    Checkpoint 04a, Classes
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary:
*    Summaries are not necessary for checkpoint assignments.
* ***********************************************************************/

#include <iostream>
#include <string>
#include "./book.h"
#include "./book.cpp"

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   Book myBook;

   myBook.prompt();
   myBook.display();
}
/********************************************************************
 * File: book.cpp
 * Purpose: Holds the implementation of the Book class methods.
 ********************************************************************/

#include "./book.h"

void Book :: display()
{
    std::cout
        << "\"" << title << "\""
        << " by " << author
        << "\n";
}

void Book :: prompt()
{
    std::cout << "Title: ";
    std::getline(std::cin, title);
    
    std::cout << "Author: ";
    std::getline(std::cin, author);
}
/********************************************************************
 * File: book.h
 * Purpose: Holds the definition of the Book class.
 ********************************************************************/

#ifndef BOOK_H
#define BOOK_H

#include <iostream>
#include <string>

/**********************************************************************
 * Class: Book
 * Purpose: Defines the structure of the "Book" class
 ***********************************************************************/
class Book
{
   private:
      std::string title;
      std::string author;

   public:
      void display();
      void prompt();
};

#endif // BOOK_H
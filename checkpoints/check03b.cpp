/***********************************************************************
* Program:
*    Checkpoint 03b, Exceptions
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary:
*    Summaries are not necessary for checkpoint assignments.
* ***********************************************************************/

#include <iostream>

/**********************************************************************
 * Function: display
 * Purpose: Display the result at the console after user input
 ***********************************************************************/
int display(int number)
{
   std::cout << "The number is " << number << ".\n";
}

/**********************************************************************
 * Function: prompt
 * Purpose: Prompts for user primary input
 ***********************************************************************/
int prompt()
{
   bool done = false;
   int number = 0;

   do
   {
      std::cout << "Enter a number: ";
      std::cin >> number;
   
      if (std::cin.fail())
      {
         std::cout << "Invalid input.\n";
         std::cin.clear();
         std::cin.ignore(1024, '\n');
      }
      else
      {
         done = true;
      }
   }
   while (!done);

   return number;
}

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   int inputNumber = prompt();
   display(inputNumber);

   return 0;
}
#include <iostream>
#include <vector>

class Position
{
    private:
        int x;
        int y;
    public:
        Position(int _x, int _y)
        {
            this->x = _x;
            this->y = _y;
        };
        int getX() const { return this->x; };
        int getY() const { return this->y; };
};

int main()
{
    char BYUI_MATRIX[4][4] = {
        {'0', '0', '0', '0'},
        {'0', '0', '0', '0'},
        {'0', '0', '0', '0'},
        {'0', '0', '0', '0'}
    };

    char TOOL_MATRIX[4][4] = {
        {'0', '0', '0', '1'},
        {'1', '1', '0', '1'},
        {'p', 'p', 'p', 'p'},
        {'p', 'p', 'p', 'w'}
    };

    Position MAP_MATRIX[4][4] = {
        { {0,0}, {3,0}, {3,1}, {0,1} },
        { {1,0}, {2,0}, {2,1}, {1,1} },
        { {3,3}, {3,3}, {3,3}, {3,3} },
        { {3,3}, {3,3}, {3,3}, {3,3} },
    };

    
    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            const int _x = MAP_MATRIX[i][j].getX();
            const int _y = MAP_MATRIX[i][j].getY();

            BYUI_MATRIX[_x][_y] = TOOL_MATRIX[i][j];
        }
    }

    std::cout << "The resulting matrix is:\n\n";

    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            std::cout
                << BYUI_MATRIX[i][j] << " ";
        }

        std::cout << "\n";
    }
}
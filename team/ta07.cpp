#include <iostream>

// Core requirements
float getValueFromPointer(float* thePointer)
{
   return (*thePointer);
}

float* getMinValue(float* a, float* b)
{
   float _a = getValueFromPointer(a);
   float _b = getValueFromPointer(b);

   if (_a < _b)
   {
      return a;
   }

   return b;
}

// Stretch goals
void swapElements(float** arr, int a, int b)
{
   float *x, *y;
   x = arr[a];
   y = arr[b];

   std::cout 
      << "pFloatArray at " << a << ": "
      << arr[a]
      << "\n";

   std::cout 
      << "pFloatArray at " << b << ": "
      << arr[b]
      << "\n";

   arr[a] = y;
   arr[b] = x;

   std::cout 
      << "pFloatArray at " << a << ": "
      << arr[a]
      << "\n";

   std::cout 
      << "pFloatArray at " << b << ": "
      << arr[b]
      << "\n";
}

void sortArray(float* arr[]);

int main()
{  
   // Core Requirement 1
   int arraySize;
   std::cout << "Enter the array size: ";
   std::cin >> arraySize;

   // Allocate your array(s) here
   float* propellerArray = new float[arraySize];

   // Fill your array with float values
   for(int i = 0; i < arraySize; i++) 
   {
      float propeller;
      std::cout << "Enter a float value: ";
      std::cin >> propeller;
      propellerArray[i] = propeller;
   }

   // Core Requirement 2
   for (int i = 0; i < arraySize; i++)
   {
      float value = getValueFromPointer(propellerArray+i);
      std::cout << "The value of the element " << i << " is: ";
      std::cout << value << "\n";
   }

   // Core Requirement 3
   // Print the smaller of the first and last elements of the array
   float *pointerToMin = getMinValue(propellerArray, propellerArray+(arraySize - 1));
   std::cout
      << "The smallest number out of the edges of the array is: "
      << (*pointerToMin)
      << "\n";


   // Stretch Challenge 1
   float** pFloatArray = new float*[arraySize];

   for(int i = 0; i < arraySize; i++) 
   {
      pFloatArray[i] = &propellerArray[i];
   }

   for(int i = 0; i < arraySize; i++) 
   {
      std::cout 
         << "The value at the original array, position " << i << ": "
         << *(*pFloatArray+i)
         << "\n";
   }

   // Stretch Challenge 2
   swapElements(pFloatArray, 0, arraySize - 1);

   for(int i = 0; i < arraySize; i++) 
   {
      std::cout 
         << "The original values array, position " << i << " still are: "
         << propellerArray[i]
         << "\n";
   }

   for(int i = 0; i < arraySize; i++) 
   {
      std::cout 
         << "The swapped values array, position " << i << " are: "
         << *(*pFloatArray+i)
         << "\n";
   }

   // Clean up your array(s) here
   delete[] propellerArray;
   delete[] pFloatArray;

   return 0;
}

/***************************************************************
 * File: robot.cpp
 * Author: Camargo, Griffin, Payne
 * Purpose: Contains the implement of the Robot class
 ***************************************************************/

#include "robot.h"
#include <iostream>

/************************************
 * Function: Robot()
 * Purpose: Default Constructor
 ************************************/
Robot :: Robot()
   :blocked(false)
{
   setEnergy(100);
   initializeLandmines();
}

/************************************
 * Function: Robot(int _energy)
 * Purpose: 1 overload
 ************************************/
Robot :: Robot(int _energy)
   :blocked(false)
{
   setEnergy(_energy);
   initializeLandmines();
}

/************************************
 * Function: Robot(int _energy, Point _point)
 * Purpose: 1 overload
 ************************************/
Robot :: Robot(Point _point, int _energy)
   :blocked(false)
{
   setEnergy(_energy);
   setPosition(_point);
   initializeLandmines();
}

/************************************
 * Function: initalizeLandmines
 * Purpose: 1 overload
 ************************************/
void Robot :: initializeLandmines()
{
    std::vector<Point> _landmines;

    _landmines.push_back(Point(3,3));
    _landmines.push_back(Point(4,4));
    _landmines.push_back(Point(6,6));

    setLandmines(_landmines);    
}

/************************************
 * Function: getPosition
 * Purpose: Gets the robot's coordinates.
 ************************************/
Point Robot :: getPosition() const
{
   return position;
}

/************************************
 * Function: getEnergy
 * Purpose: Gets the robot's energy.
 ************************************/
int Robot :: getEnergy() const
{
   return energy;
}

/************************************
 * Function: getBlocked
 * Purpose: Gets the robot's action state.
 ************************************/
bool Robot :: getBlocked() const
{
   return blocked;
}

/************************************
 * Function: getLandmines
 * Purpose: Gets landmines coordinates.
 ************************************/
std::vector<Point> Robot :: getLandmines()
{
   return landmines;
}

/************************************
 * Function: setPosition
 * Purpose: Set the robot's coordinates.
 ************************************/
void Robot :: setPosition(const Point _point)
{
   position = _point;
}

/************************************
 * Function: setEnergy
 * Purpose: Set the robot's energy.
 ************************************/
void Robot :: setEnergy(int _energy)
{
   if (_energy <= 0)
   {
      energy = 0;
      setBlocked(true);
   }
   else
   {
      energy = _energy;
   }
}

/************************************
 * Function: setBlocked
 * Purpose: Sets the robot's action state.
 ************************************/
void Robot :: setBlocked(bool _blocked)
{
   blocked = _blocked;
}

/************************************
 * Function: setLandmine
 * Purpose: Sets landmine coordinates.
 ************************************/
void Robot :: setLandmines(std::vector<Point> _landmines)
{
   landmines = _landmines;
}

/************************************
 * Function: display
 * Purpose: Displays the robot.
 ************************************/
void Robot :: display() const
{
   position.display();
   std::cout << " - Energy: " << getEnergy();
}

/************************************
 * Function: displayNotEnoughEnergy
 * Purpose: Displays the "Not Enought Energy" error.
 ************************************/
void Robot :: displayNotEnoughEnergy() const
{
   std::cout << "You don't have enough energy for the selected action.\n";
}

/************************************
 * Function: moveUp
 * Purpose: Moves the robot up one position
 ************************************/
bool Robot :: moveUp()
{
   if (getEnergy() < 10)
   {
      // Throws a "NotEnoughEnergy" Exception
      displayNotEnoughEnergy();
      return false;
   }

   if (getBlocked())
   {
      return abortAction();
   }

   setEnergy(getEnergy() - 10);
   position.setY(position.getY()-1);

   checkLandmine();

   return true;
}

/************************************
 * Function: moveDown
 * Purpose: Moves the robot down one position
 ************************************/
bool Robot :: moveDown()
{
   if (getEnergy() < 10)
   {
      // Throws a "NotEnoughEnergy" Exception
      displayNotEnoughEnergy();
      return false;
   }

   if (getBlocked())
   {
      return abortAction();
   }

   setEnergy(getEnergy() - 10);
   position.setY(position.getY()+1);

   checkLandmine();

   return true;
}

/************************************
 * Function: moveLeft
 * Purpose: Moves the robot left one position
 ************************************/
bool Robot :: moveLeft()
{
   if (getEnergy() < 10)
   {
      // Throws a "NotEnoughEnergy" Exception
      displayNotEnoughEnergy();
      return false;
   }

   if (getBlocked())
   {
      return abortAction();
   }

   setEnergy(getEnergy() - 10);
   position.setX(position.getX()-1);

   checkLandmine();

   return true;
}   

/************************************
 * Function: moveRight
 * Purpose: Moves the robot right one position
 ************************************/
bool Robot :: moveRight()
{   
   if (getEnergy() < 10)
   {
      // Throws a "NotEnoughEnergy" Exception
      displayNotEnoughEnergy();
      return false;
   }

   if (getBlocked())
   {
      return abortAction();
   }

   setEnergy(getEnergy() - 10);
   position.setX(position.getY()+1);

   checkLandmine();

   return true;
}

/**************************************
* Function: fireLaser
* Purpose: Imma firin my laser
**************************************/
bool Robot::fireLaser()
{
   if (getEnergy() < 25)
   {
      // Throws a "NotEnoughEnergy" Exception
      displayNotEnoughEnergy();
      return false;
   }

   if (getBlocked())
   {
      return abortAction();
   }

   setEnergy(getEnergy() - 25);
   std::cout << "Pew! Pew!";
   return true;
}

/************************************
 * Function: abortAction
 * Purpose: Aborts the current pending action.
 ************************************/
bool Robot :: abortAction()
{
   std::cout
      << "Your energy has been reduced to zero, and therefore, "
      << "you cannot perform any actions."
      << "\n";

   return false;
}

/************************************
 * Function: checkLandmine
 * Purpose: Checks if we hit a landmine.
 ************************************/
void Robot :: checkLandmine()
{
   std::vector<Point>::iterator it;
   Point p;

   for (it = landmines.begin(); it != landmines.end(); it++)
   {
      if ( (*it).getX() == getPosition().getX()
         && (*it).getY() == getPosition().getY() )
      {
         hitLandmine();
      }
   }
}

/************************************
 * Function: hitLandmine
 * Purpose: Handles movement in a landmine position.
 ************************************/
void Robot :: hitLandmine()
{
   std::cout
      << "You have hit a landmine."
      << "\n"
      << "Your energy has been reduced to 0."
      << "\n";

   setEnergy(0);
}
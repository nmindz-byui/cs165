/***************************************************************
 * File: robot.h
 * Author: Camargo, Griffin, Payne
 * Purpose: Contains the definition of the Robot class
 ***************************************************************/

#ifndef ROBOT_H
#define ROBOT_H

#include "point.h"
#include <vector>

class Robot
{
   private:
      Point position;
      int energy;
      bool blocked;
      std::vector<Point> landmines;
      void initializeLandmines();
   public:
      // Constructors
      Robot();
      Robot(int _energy);
      Robot(Point _point, int _energy);
      // Getters
      int getEnergy() const;
      Point getPosition() const;
      bool getBlocked() const;
      std::vector<Point> getLandmines();
      // Setters
      void setEnergy(int value);
      void setPosition(const Point _point);
      void setBlocked(const bool _blocked);
      void setLandmines(const std::vector<Point> _landmines);
      // Misc
      void display() const;
      void displayNotEnoughEnergy() const;
      bool abortAction();
      // Actions
	   bool fireLaser();
      bool moveUp();
      bool moveDown();
      bool moveLeft();
      bool moveRight();
      // Triggers
      void checkLandmine();
      void hitLandmine();
};

#endif // ROBOT_H
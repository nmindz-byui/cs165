/***************************************************************
 * File: point.h
 * Author: Camargo, Griffin, Payne
 * Purpose: Contains the definition of the Point class
 ***************************************************************/

#ifndef POINT_H
#define POINT_H

class Point
{
   private:
      int x;
      int y;

   public:
      // Constructors
      Point();
      Point(int _x, int _y);
      // Getters
      int getX() const;
      int getY() const;
      // Setters
      void setX(int value);
      void setY(int value);
      // Misc
      void display() const;
};

#endif // POINT_H

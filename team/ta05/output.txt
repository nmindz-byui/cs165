This point should be: (6, 7). It is: (6, 7)
Your point is x=6 and y=7
This one should be: (1, 10). It is: (1, 10)
Testing the robot, first with -10 energy, then with 100.
(1, 10) - Energy: 0
(1, 10) - Energy: 100
Default Point constructor. It should be: (5, 5). It is: (5, 5)
Non-default Point constructor. It should be: (3, 7). It is: (3, 7)
Non-default Point constructor. It should be: (10, 1). It is: (10, 1)
Default constructor. Should be: (5, 5) - 100. It is: (5, 5) - Energy: 100
Non-default constructor 1. Should be: (5, 5) - 13. It is: (5, 5) - Energy: 13
Non-default constructor 1. Should be: (5, 5) - 0. It is: (5, 5) - Energy: 0
Non-default constructor 2. Should be: (7, 3) - 19. It is: (7, 3) - Energy: 19
Enter a command (u=up, d=down, l=left, r=right, f=fire, q=quit): 
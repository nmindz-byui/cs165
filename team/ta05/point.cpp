/***************************************************************
 * File: point.cpp
 * Author: Camargo, Griffin, Payne
 * Purpose: Contains the implement of the Point class
 ***************************************************************/

#include <iostream>
#include "point.h"

/******************************************
 * Function: Point()
 * Purpose: Default Constructor
 ******************************************/
Point :: Point()
{
    setX(5);
    setY(5);
}

/******************************************
 * Function: Point(int _x, int _y)
 * Purpose: 2 overloads
 ******************************************/
Point :: Point(int _x, int _y)
{
    setX(_x);
    setY(_y);
}

/******************************************
 * Function: getX
 * Purpose: Gets the X the point
 ******************************************/
int Point :: getX() const
{
    return x;
}

/******************************************
 * Function: getY
 * Purpose: Gets the Y the point
 ******************************************/
int Point :: getY() const
{
   return y;
}

/******************************************
 * Function: setX
 * Purpose: Sets the X the point
 ******************************************/
void Point :: setX(int _value)
{
    x = _value;
    
    if (_value < 1)
    {
        x = 1;
    }
    else if (_value > 10)
    {
        x = 10;
    }
}

/******************************************
 * Function: setY
 * Purpose: Sets the Y the point
 ******************************************/
void Point :: setY(int _value)
{
    y = _value;

    if (_value < 1)
    {
        y = 1;
    }
    else if (_value > 10)
    {
        y = 10;
    }
}

/******************************************
 * Function: display
 * Purpose: Displays the point
 ******************************************/
void Point :: display() const
{
   // Note: We could just use x and y here and not the getters
   // because it's a member function. But this will force you
   // to deal with const :-)...

   std::cout << "(" << getX() << ", " << getY() << ")";
}

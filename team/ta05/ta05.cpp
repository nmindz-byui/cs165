/***************************************************************
 * File: ta05.cpp
 * Author: Camargo, Griffin, Payne
 * Purpose: Contains the implement of the Point class
 ***************************************************************/

#include <iostream>
#include <string>
#include "robot.h"
#include "point.h"

/******************************************
 * Function: partI
 * Purpose: Tests the PART I of the requirement set
 ******************************************/
void partI()
{
   // PART I - Getters and Setters
   
   // Test the point
   Point p;

   p.setX(6);
   p.setY(7);

   std::cout << "This point should be: (6, 7). It is: ";
   p.display();
   std::cout << "\n";

   std::cout << "Your point is x=" << p.getX() << " and y=" << p.getY() << "\n";

   p.setX(-10);
   p.setY(15);

   std::cout << "This one should be: (1, 10). It is: ";
   p.display();
   std::cout << "\n";

   // Test the Robot:
   Robot robot;
   robot.setPosition(p);
   robot.setEnergy(-10);

   std::cout << "Testing the robot, first with -10 energy, then with 100.\n";
   robot.display();
   std::cout << "\n";

   robot.setEnergy(100);
   robot.display();
   std::cout << "\n";
}

/******************************************
 * Function: partII
 * Purpose: Tests the PART II of the requirement set
 ******************************************/
void partII()
{
   // PART II - Point Constructors
   Point p1;
   std::cout << "Default Point constructor. It should be: (5, 5). It is: ";
   p1.display();
   std::cout << "\n";

   Point p2(3, 7);
   std::cout << "Non-default Point constructor. It should be: (3, 7). It is: ";
   p2.display();
   std::cout << "\n";

   Point p3(15, -5);
   std::cout << "Non-default Point constructor. It should be: (10, 1). It is: ";
   p3.display();
   std::cout << "\n";
}

/******************************************
 * Function: partIII
 * Purpose: Tests the PART III of the requirement set
 ******************************************/
void partIII()
{
   // PART III - Robot Constructors
   Robot r1;
   std::cout << "Default constructor. Should be: (5, 5) - 100. It is: ";
   r1.display();
   std::cout << "\n";

   Robot r2(13);
   std::cout << "Non-default constructor 1. Should be: (5, 5) - 13. It is: ";
   r2.display();
   std::cout << "\n";

   Robot r3(-50);
   std::cout << "Non-default constructor 1. Should be: (5, 5) - 0. It is: ";
   r3.display();
   std::cout << "\n";

   Point p(7, 3);
   Robot r4(p, 19);
   std::cout << "Non-default constructor 2. Should be: (7, 3) - 19. It is: ";
   r4.display();
   std::cout << "\n";
}

/******************************************
 * Function: stretch
 * Purpose: Tests the STRETCH challenges
 ******************************************/
void stretch()
{
   
   // STRETCH PART I
   Robot robot;

   char command;

   do
   {
      std::cout << "Enter a command (u=up, d=down, l=left, r=right, f=fire, q=quit): ";
      std::cin >> command;

      switch (command)
      {
         case 'u':
            std::cout << "Moving up...\n";
            robot.moveUp();
            break;

         case 'd':
            std::cout << "Moving down...\n";
            robot.moveDown();
            break;

         case 'l':
            std::cout << "Moving left...\n";
            robot.moveLeft();
            break;

         case 'r':
            std::cout << "Moving right...\n";
            robot.moveRight();
            break;

         case 'f':
            std::cout << "Firing the laser!\n";
            
            // STRETCH PART II - Uncomment the next line
            robot.fireLaser();
            break;

         default:
            std::cout << "Command not recognized.\n";
            break;
      }

      robot.display();
      std::cout << "\n";

   } while (command != 'q');

   
}

/******************************************
 * Function: main
 * Purpose: The application entrypoint
 ******************************************/
int main()
{
   partI();
   partII();
   partIII();

   stretch();

   return 0;
}

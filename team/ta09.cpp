/********************************************************
 * Team activity lesson 09
 * 
 * ******************************************************/

#include <iostream>
#include <string>

/**********************************************************************
 * Class: Employee
 * Purpose: Defines the structure of the "Employee" class
 ***********************************************************************/

#ifndef EMPLOYEE_H
#define EMPLOYEE_H

class Employee
{
	private:
		std::string mName;
	public:
		std::string getName() const
		{
			return mName;
		}
		void setName(const std::string name)
		{
			mName = name;
		}
		virtual void display() const
		{
			std::cout << mName << std::endl;			
		}
};

#endif // EMPLOYEE_H

/**********************************************************************
 * Class: HourlyEmployee
 * Purpose: Defines the structure of the "HourlyEmployee" class
 ***********************************************************************/

#ifndef HOURLY_EMPLOYEE_H
#define HOURLY_EMPLOYEE_H

class HourlyEmployee
	: public Employee
{
	private:
		int mHourlyWage;
	public:
      HourlyEmployee() : mHourlyWage(0) { }
      HourlyEmployee(std::string _name, int _hourlyWage)
      {
         setName(_name);
         setHourlyWage(_hourlyWage);
      }
		int getHourlyWage() const
		{
			return mHourlyWage;
		}
		void setHourlyWage(int _hourlyWage)
		{
			mHourlyWage = _hourlyWage;
		}
		void display() const
		{
			std::cout 
				<< getName() 
				<< " - $" 
				<< getHourlyWage() 
				<< "/hour" 
				<< std::endl;
		}				
};

#endif // HOURLY_EMPLOYEE_H

/**********************************************************************
 * Class: SalaryEmployee
 * Purpose: Defines the structure of the "SalaryEmployee" class
 ***********************************************************************/

#ifndef SALARY_EMPLOYEE_H
#define SALARY_EMPLOYEE_H

class SalaryEmployee : public Employee
{
	private:
		int salary;
	public:
      SalaryEmployee() : salary(0) { }
      SalaryEmployee(std::string _name, int _salary)
      {
         setName(_name);
         setSalary(_salary);         
      }
		int getSalary() const
		{
			return salary;
		}
		void setSalary(const int _salary)
		{
			salary = _salary;
		}
      void display()
      {
         std::cout
            << getName()
            << " - $"
            << getSalary()
            << "/year\n";
      }      
};

#endif // SALARY_EMPLOYEE_H


int main()
{
   char type;
	Employee *employees[3];
   
	for (int i = 0; i < 3; i++)
   {
      std::cout
         << "Is the employee paid hourly or on salary? (h/s) ";
      std::cin >> type;

      std::string _name;
      int _value;
      
      std::cout << "Enter the name of the employee: ";
      

      std::cout << "Input the value of the hourly/salary payment: ";
      std::cin >> _value;

      switch(type)
      {
         case 'h':
			   {
               HourlyEmployee* empH = new HourlyEmployee();
               empH->setName(_name);
               empH->setHourlyWage(_value);

               employees[i] = empH;
			   }
            break;
         case 's':
			   {
               SalaryEmployee* empS = new SalaryEmployee();
               empS->setName(_name);
               empS->setSalary(_value);

               employees[i] = empS;
			   }
            break;
         default:
            // do nothing. alright
            break;
      }
   }

   for (int i = 0; i < 3; i++)
   {
      employees[i]->display();
      delete employees[i];
   }

	return 0;	
}
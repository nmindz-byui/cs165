/********************************************************************
 * File: rational.h
 * Purpose: Holds the definition of the Rational class.
 ********************************************************************/

#ifndef RATIONAL_H
#define RATIONAL_H

#include <string>

/**********************************************************************
 * Class: Rational
 * Purpose: Defines the structure of the "Rational" class
 ***********************************************************************/

class Rational
{
   private:
      int top;
      int bottom;
      int remainder;
      int base;
   public:
      void prompt();
      void display();
      void displayDecimal();
      void multiplyBy(Rational fraction);
};

#endif // RATIONAL_H
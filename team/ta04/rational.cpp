/********************************************************************
 * File: rational.cpp
 * Purpose: Holds the implementation of the Rational class.
 ********************************************************************/

#include <iostream>
#include "./rational.h"

/**********************************************************************
 * Function: prompt
 * Purpose: Prompts for top/bottom input
 ***********************************************************************/
void Rational :: prompt()
{
   std::cout << "Top: ";
   std::cin >> top;
   std::cout << "Bottom: ";
   std::cin >> bottom;
   base = bottom;
}

/**********************************************************************
 * Function: displayDecimal
 * Purpose: Casts, divides and displays the result with decimal places
 ***********************************************************************/
void Rational :: displayDecimal()
{
   double _top = top;
   double _bottom = bottom;
   std::cout << _top/_bottom;
}

/**********************************************************************
 * Function: display
 * Purpose: Divides and displays the result without decimal places
 ***********************************************************************/
void Rational :: display()
{
   if (top % bottom == 0)
   {
      std::cout
         << top/bottom << "\n";
   }
   else
   {
      std::cout 
         << top / bottom << " "
         << top % bottom << "/"
         << bottom << "\n";
   }
}

/**********************************************************************
 * Function: multiplyBy
 * Purpose: Multiplies the rational number by a given fraction
 ***********************************************************************/
void Rational :: multiplyBy(Rational fraction)
{
  this->top *= fraction.top;
  this->bottom *= fraction.bottom;
}

/**************************************************************
 * File: ta04.cpp
 * Author: Camargo, Griffin, Payne
 * Purpose: Contains the main function to test the Rational class.
 ***************************************************************/

#include <iostream>
#include <string>
#include "./rational.h"
#include "./rational.cpp"

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   // Declare your Rational object here
   Rational rNumber;
   // Call your prompt function here
   rNumber.prompt();
   // Call your display functions here
   rNumber.display();
   rNumber.displayDecimal();
   
   Rational rational2;
   rational2.prompt();

   rNumber.multiplyBy(rational2);

   std::cout << "\n";
   std::cout << "Product: ";
   rNumber.display();
   
   return 0;
}
#include <iostream>
#include "circle.h"
#include "point.h"

int Circle :: getRadius() const
{
	return mRadius;
}

void Circle :: setRadius(int radius)
{
	mRadius = radius;
}

void Circle::display()
{
	Point::display();
	std :: cout << " - Radius: " << mRadius << std :: endl;
}

void Circle :: promptForCircle()
{
   Point::promptForPoint();

   std::cout << "Enter radius: ";
   std::cin >> mRadius;
}
#ifndef CIRCLE_H

#include "point.h"

class Circle
   : public Point
{
   private:
      int mRadius;
   public:
      int getRadius() const;
      void setRadius(int radius);
      void promptForCircle();
      void display();

};
#endif 
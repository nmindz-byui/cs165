/**************************************************************
* Program:
*     Team Activity 10, Vectors
*     Brother Dudley, CS165
* Author:
*     Camargo, Griffin, Payne
* Summary:
*     Week 10 Team Work 
**************************************************************/
#include <iostream>
#include <vector>

/****************************************************
 * Function: promptNumbers
 * Description: Prompts the user for numbers and adds
 * the input to the end of the vector.
 ****************************************************/
void promptNumbers(std::vector<int> & numbers)
{
   int _input;

   do
   {
      std::cout << "Enter a number (0 to quit): ";
      std::cin >> _input;

      if (_input != 0)
      {  
         numbers.push_back(_input);
      }
   }
   while (_input != 0);
}

/**********************************************************
 * Function: displayNumbers
 * Description: Displays numbers in the vector
 **********************************************************/
void displayNumbers(std::vector<int> & numbers)
{
   std::cout << "\nThe contents of the vector are:\n";

   for (std::vector<int>::iterator it = numbers.begin(); it != numbers.end(); it++)
   {
      std::cout << *it << "\n";
   }
}

/**********************************************************
 * Function: removeOdds
 * Description: Removes all odd numbers
 **********************************************************/
void removeOdds(std::vector<int> & numbers)
{
   std::cout << "\nRemoving odd numbers...\n";

   std::vector<int>::iterator it = numbers.begin();
   
   while (it != numbers.end())
   {
      if (*it % 2 != 0)
      {
         it = numbers.erase(it);
      }
      else
      {
         it++;
      }
   }
}

/**********************************************************
 * Function: main
 * Description: Application Entrypoint
 **********************************************************/
int main()
{
   std::vector<int> numbers;
   promptNumbers(numbers);
   displayNumbers(numbers);
   removeOdds(numbers);
   displayNumbers(numbers);

   return 0;
}
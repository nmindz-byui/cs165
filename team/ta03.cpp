/**********************************************************************
 * File: ta03.cpp
 * Author: Br. Burton
 *
 * Description: Use this file as a starting point for Team Activity 03.
 *    You do not need to "submit" your code, but rather, answer the
 *    questions in the I-Learn assessment.
 **********************************************************************/

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>

/***********************************************************************
 * Struct: Scripture
 * Description: Holds the reference to a scripture (book, chapter, verse
 ***********************************************************************/
struct Scripture
{
   std::string book;
   int chapter;
   int firstVerse;
   int secondVerse;
};

/***********************************************************************
 * Function: display
 * Description: Displays the passed scripture.
 ***********************************************************************/
void display(const Scripture &scripture)
{
   std::cout << scripture.book << " ";
   std::cout << scripture.chapter << ":";
   std::cout << scripture.firstVerse;
   
   if (scripture.secondVerse != NULL)
   {
      std::cout << scripture.chapter << "-";
      std::cout << scripture.secondVerse;
   }

   std::cout << "\n";
}

/***********************************************************************
 * Function: prompt
 * Description: Prompt for the file name.
 ***********************************************************************/
void prompt(std::string &fileName)
{
   bool done = false;

   do
   {
      std::cout << "Enter the name of the file: ";

      if (std::getline(std::cin, fileName))
      {
         throw "Generic I/O error!";
      }
      else if (fileName.empty())
      {
         throw "No file was specified!";
      }
      else
      {
         done = true;
      }
   }
   
   while (!done);
}

/***********************************************************************
 * Function: readFile
 * Description: Reads the specified file.
 ***********************************************************************/
void readFile(std::string &fileName)
{
   std::vector<Scripture> scriptures;
   std::ifstream file(fileName);

   std::string line;
   std::string a;
   int b = 0;
   int c = 0;
   int d = 0;

   while (std::getline(file, line))
   {
      std::stringstream scriptureLine(line);
      while (scriptureLine >> a >> b >> c >> d)
      {
         scriptures.push_back({ a, b, c, d });
      }
   }
}

/***********************************************************************
 * Function: main
 * Description: The driver for the program.
 ***********************************************************************/
int main()
{
   // Insert your code here to prompt for the name of a file
   // and pass it to a readFile function

   std::string fileName;

   try
   {
      prompt(fileName);
   }
   catch (const char * message)
   {
      std::cout << "Error: " << message << "\n";
   }

   return 0;
}

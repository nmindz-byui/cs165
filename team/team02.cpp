/***********************************************************************
* Program:
*    Teach 02, Team Activity - Structs
*    Brother Dudley, CS165
* Author:
*    Evandro Camargo
* Summary:
*    As a team, create a struct for a scripture that contains a book,
*    chapter, and verse. Then, write a function to display a scripture, 
*    and one to prompt for a scripture.
*
* ***********************************************************************/

#include <iostream>
#include <string>
#include <vector>

struct Scripture {
    std::string bookName;
    int chapter;
    int verse;
};

struct Insight {
   Scripture reference;
   std::string message;
};

/**********************************************************************
 * Function: promptScripture
 * Purpose: Prompts the user for a verse of scripture
 ***********************************************************************/
void promptScripture(Scripture &scrpt)
{
   std::cout << "Please enter the desired book name: ";
   std::getline(std::cin, scrpt.bookName);
   
   std::cout << "Please enter the desired chapter: ";
   std::cin >> scrpt.chapter;
   std::cin.ignore(1024, '\n');
   
   std::cout << "Please enter the desired verse: ";
   std::cin >> scrpt.verse;
   std::cin.ignore(1024, '\n');

   std::cout << "\n";

   return;
}

/**********************************************************************
 * Function: displayScripture
 * Purpose: Displays a verse of scripture
 ***********************************************************************/
void displayScripture(Scripture &scrpt)
{
   std::cout << "\""
      << scrpt.bookName << " "
      << scrpt.chapter << ":"
      << scrpt.verse << "\""
      << "\n";

   return;
}

/**********************************************************************
 * Function: promptInsight
 * Purpose: Prompts the user for insight on a scripture
 ***********************************************************************/
void promptInsight(Insight &ins)
{
   std::cout
      << "Please enter the insight for "
      << ins.reference.bookName << " "
      << ins.reference.chapter << ":"
      << ins.reference.verse
      << " : ";

   std::getline(std::cin, ins.message);
   std::cout << "\n";

   return;
}

/**********************************************************************
 * Function: displayInsight
 * Purpose: Displays the insight of a scripture
 ***********************************************************************/
void displayInsight(Insight &ins)
{
   std::cout
      << "The insight for this verse is: "
      << ins.message
      << "\n" << "\n";

   return;
}

/**********************************************************************
 * Function: main
 * Purpose: This is the entry point and driver for the program.
 ***********************************************************************/
int main()
{
   Scripture scrptArray[3];
   Insight insArray[3];

   for (int i = 0; i < 3; i++)
   {
      promptScripture(scrptArray[i]);
      insArray[i].reference = scrptArray[i];
      promptInsight(insArray[i]);
   }

   std::cout << "################################\n";
   std::cout << "## The chosen scriptures are: ##\n";
   std::cout << "################################\n\n";
   for (int i = 0; i < 3; i++)
   {
      displayScripture(scrptArray[i]);
      displayInsight(insArray[i]);
   }

   return 0;
}